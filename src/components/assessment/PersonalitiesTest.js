import React, { Component } from 'react';
import { getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Media, Card, Button, Row, Col } from 'reactstrap';

import PersonalitiesForm from '../../components/forms/assessment/PersonalitiesForm'
import checked from '../../assets/img/assessment/checked.svg'

import { setPersonalities, changePersonalitiesPage, changeValuesPage, clearPersonalities } from '../../actions'

class PersonalitiesTest extends Component {

    constructor(props) {
        super(props) 

        this.clearPage = this.clearPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }
    
    clearPage() {

        let { clearPersonalities, changePersonalitiesPage, values, valuesPage, changeValuesPage } = this.props

        clearPersonalities()
        changePersonalitiesPage(0)

        if (valuesPage === 0) {
            if(_.isEmpty(values)){
                changeValuesPage(valuesPage+1);
            }
        }
    }

    nextPage() {
        let { changePersonalitiesPage, page } = this.props

        changePersonalitiesPage(page+1)
    }

    previousPage() {
        let { changePersonalitiesPage, page } = this.props

        changePersonalitiesPage(page-1)
    }

    submit = (result) => {

        let { setPersonalities, changePersonalitiesPage, values, valuesPage, changeValuesPage } = this.props

        setPersonalities(result)
        changePersonalitiesPage(0)

        if (valuesPage === 0) {
            if(_.isEmpty(values)){
                changeValuesPage(valuesPage+1);
            }
        }

        console.log(result)
    }


    render() {

        let { page, formValues, personalities, test } = this.props

        return (
            <Card>
                {page === 0 && 
                    <Row className="align-items-center">
                        <Col lg="9" md="6" xs="12">
                            <Media>
                                {_.isEmpty(personalities) ? null : <Media left><img src={checked} className="checked" alt="checked"/></Media>}
                                <Media body>
                                    <div className="assessment-title">Personalities</div>
                                    <div className="assessment-time">Approximate passing time 1 min 48 sec</div>
                                </Media>
                            </Media>
                        </Col>
                        <Col lg="3" md="6" xs="12">
                            <div className="float-md-right float-sm-left">
                                <Button color="primary" size="lg" onClick={this.nextPage}>{formValues ? "Edit answers" : "Start test"}</Button>
                            </div> 
                        </Col>
                    </Row>}
                {
                    page === 1 && 
                    <PersonalitiesForm amount={1} current={1} 
                        pageWithTest={test} previousPage={this.previousPage} 
                        onSubmit={this.submit} 
                        clearPage={this.clearPage}/>
                }
            </Card>                  
        );
    }
}

const mapStateToProps = (state) => {
    return {
        formValues: getFormValues('personalitiesForm')(state),
        personalities: state.assessment.personalities,
        values: state.assessment.values,
        page: state.assessment.pages.personalities,
        valuesPage: state.assessment.pages.values
    }
}

export default connect(
    mapStateToProps,
    {
        setPersonalities, changePersonalitiesPage, changeValuesPage, clearPersonalities
    }
)(PersonalitiesTest)
