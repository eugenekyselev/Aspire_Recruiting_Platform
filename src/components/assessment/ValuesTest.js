import React, { Component } from 'react';
import { getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Media, Card, Button, Row, Col } from 'reactstrap';

import ValuesForm from '../../components/forms/assessment/ValuesForm'
import checked from '../../assets/img/assessment/checked.svg'

import { setValues, changeValuesPage, clearValues } from '../../actions'

class ValuesTest extends Component {

    constructor(props) {
        super(props) 

        this.clearPage = this.clearPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }
    
    clearPage() {

        let { clearValues, changeValuesPage } = this.props

        clearValues()
        changeValuesPage(0)
    }

    nextPage() {
        let { changeValuesPage, page } = this.props

        changeValuesPage(page+1)
    }

    previousPage() {
        let { changeValuesPage, page } = this.props

        changeValuesPage(page-1)
    }

    submit = (result) => {

        let { setValues, changeValuesPage } = this.props

        setValues(result)
        changeValuesPage(0)

        console.log(result)
    }


    render() {

        let { page, formValues, values, test } = this.props

        return (
            <Card>
                {page === 0 && 
                    <Row className="align-items-center">
                        <Col lg="9" md="6" xs="12">
                            <Media>
                                {_.isEmpty(values) ? null : <Media left><img src={checked} className="checked" alt="checked"/></Media>}
                                <Media body>
                                    <div className="assessment-title">Values</div>
                                    <div className="assessment-time">Approximate passing time 2 min 32 sec</div>
                                </Media>
                            </Media>
                        </Col>
                        <Col lg="3" md="6" xs="12">
                            <div className="float-md-right float-sm-left">
                                <Button color="primary" size="lg" onClick={this.nextPage}>{formValues ? "Edit answers" : "Start test"}</Button>
                            </div> 
                        </Col>
                    </Row>}
                {_.chunk(test, 12).map((pageWithTest, i) => (
                    page === i+1 && 
                    <ValuesForm amount={_.chunk(test, 12).length} allAmount={test.length} current={i+1} 
                        key={i} pageWithTest={pageWithTest} previousPage={this.previousPage} 
                        onSubmit={_.chunk(test, 12).length === i+1 ? this.submit : this.nextPage} 
                        clearPage={this.clearPage}/>
                ))}
            </Card>                  
        );
    }
}

const mapStateToProps = (state) => {
    return {
        formValues: getFormValues('valuesForm')(state),
        values: state.assessment.values,
        page: state.assessment.pages.values
    }
}

export default connect(
    mapStateToProps,
    {
        setValues, changeValuesPage, clearValues
    }
)(ValuesTest)
