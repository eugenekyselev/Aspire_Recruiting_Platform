import React, { Component } from 'react';
import { getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Media, Card, Button, Row, Col } from 'reactstrap';

import CultureForm from '../../components/forms/assessment/CultureForm'
import checked from '../../assets/img/assessment/checked.svg'

import { setCulture, changeCulturePage, changePersonalitiesPage, clearCulture } from '../../actions'

class CultureTest extends Component {

    constructor(props) {
        super(props) 

        this.clearPage = this.clearPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }
    
    clearPage() {

        let { clearCulture, changeCulturePage, personalitiesPage, changePersonalitiesPage, personalities } = this.props

        clearCulture()
        changeCulturePage(0)

        if (personalitiesPage === 0) {
            if(_.isEmpty(personalities)){
                changePersonalitiesPage(personalitiesPage+1);
            }
        }
    }

    nextPage() {
        let { changeCulturePage, page } = this.props

        changeCulturePage(page+1);
    }

    previousPage() {

        let { changeCulturePage, page } = this.props

        changeCulturePage(page-1);
    }
    

    submit = (result) => {

        let { setCulture, changeCulturePage, personalitiesPage, changePersonalitiesPage, personalities } = this.props

        setCulture(result)
        changeCulturePage(0)

        if (personalitiesPage === 0) {
            if(_.isEmpty(personalities)){
                changePersonalitiesPage(personalitiesPage+1);
            }
        }

        console.log(result)
    }


    render() {

        let { page, formValues, culture, test } = this.props

        return (
            <Card>
                {page === 0 && 
                    <Row className="align-items-center">
                        <Col lg="9" md="6" xs="12">
                            <Media>
                                {_.isEmpty(culture) ? null : <Media left><img src={checked} className="checked" alt="checked"/></Media>}
                                <Media body>
                                    <div className="assessment-title">Culture</div>
                                    <div className="assessment-time">Approximate passing time 2 min 32 sec</div>
                                </Media>
                            </Media>
                        </Col>
                        <Col lg="3" md="6" xs="12">
                            <div className="float-md-right float-sm-left">
                                <Button color="primary" size="lg" onClick={this.nextPage}>{formValues ? "Edit answers" : "Start test"}</Button>
                            </div> 
                        </Col>
                    </Row>}
                {test.map((pageWithTest, i) => (
                    page === i+1 && 
                    <CultureForm amount={test.length} current={i+1} 
                        key={i} pageWithTest={pageWithTest} previousPage={this.previousPage} 
                        onSubmit={test.length === i+1 ? this.submit : this.nextPage} 
                        clearPage={this.clearPage}/>
                ))}
            </Card>                  
        );
    }
}

const mapStateToProps = (state) => {
    return {
        formValues: getFormValues('cultureForm')(state),
        culture: state.assessment.culture,
        personalities: state.assessment.personalities,
        page: state.assessment.pages.culture,
        personalitiesPage: state.assessment.pages.personalities
    }
}

export default connect(
    mapStateToProps,
    {
        setCulture, changeCulturePage, changePersonalitiesPage, clearCulture
    }
)(CultureTest)
