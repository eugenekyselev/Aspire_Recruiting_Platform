import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button} from 'reactstrap';

import star from "../../assets/img/jobs_list/shape.svg"
import selectedStar from "../../assets/img/jobs_list/shape-active.svg"
import CardWithTitleAndDescriptionAndCharts from "../jobs/jobs_list/CardWithTitleAndDescriptionAndCharts";

class InviteToApplyHired extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isInFavourite: this.props.isInFavourite
        };
    }

    componentDidMount() {
        this.setState(prevState => ({
            shouldRerender: false
        }));
    }

    toggle = () => {
        this.setState(prevState => ({
            isInFavourite: !prevState.isInFavourite
        }));
        this.props.favouriteClicked(this.state.isInFavourite, this.props.card.user_id, 'applied')

    }

    render() {

        let card = this.props.card;

        return (
            <Container className="pl-2 pb-3 pr-3">
                <CardWithTitleAndDescriptionAndCharts shouldRerender={this.state.shouldRerender} animated={true} chartWidth={45} chartHeight={45} percentage={card.percentage} type={this.props.type} className="pb-1" height={this.props.height} innerSize={this.props.innerSize} card={this.props.card}/>
                <div className="hired-buttons pt-9px">
                    <Button color="primary" className="invite-to-apply-button rounded self-left border-0">Invite to apply</Button>
                    <button type="submit" className="transparent-button self-right disable-border" onClick={this.toggle}>
                        <img src={this.state.isInFavourite ? selectedStar : star} alt="..." className="star"/>
                    </button>
                </div>
            </Container>
        );
    }
}

export default InviteToApplyHired;