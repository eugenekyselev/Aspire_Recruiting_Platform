import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button} from 'reactstrap';

import star from "../../assets/img/jobs_list/shape.svg"
import selectedStar from "../../assets/img/jobs_list/shape-active.svg"
import CardWithTitleAndDescriptionAndCharts from "../jobs/jobs_list/CardWithTitleAndDescriptionAndCharts";

class MakeOfferOrDeclinePerson extends Component {

    render() {

        let card = this.props.card;

        return (
            <Container className="pl-2 pb-3 pr-3">
                <CardWithTitleAndDescriptionAndCharts animated={true} chartWidth={45} chartHeight={45} percentage={card.percentage} type={this.props.type} className="pb-1" height={this.props.height} innerSize={this.props.innerSize} card={this.props.card}/>
                <div className="accept-decline-buttons pt-9px">
                    <Button color="primary" className="make-offer-button rounded self-left border-0">Make offer</Button>
                    <div className="empty-div"></div>
                    <Button color="primary" className="decline-button rounded self-left">Decline</Button>
                </div>
            </Container>
        );
    }
}

export default MakeOfferOrDeclinePerson;