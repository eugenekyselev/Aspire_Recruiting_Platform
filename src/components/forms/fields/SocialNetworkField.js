import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, getFormValues } from 'redux-form';
import Dropzone from 'react-dropzone';
import { Row, Col, ButtonToolbar, Button, FormGroup, Input, InputGroup, InputGroupAddon, Label } from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import { MdAdd } from 'react-icons/md';

import upload from '../../../assets/upload.svg'
import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

import { MdClear, MdModeEdit } from "react-icons/md";

class SocialNetworkField extends Component {

    constructor(props) {
        super(props) 

        this.state = {

            social: _.get(props.formValues, props.name, ""),
            socialErr:"",
            socialFieldActivate: _.get(props.formValues, props.name, false)
        }
    }

    addSocial = (values) => () => {

        let { addSocial } = this.props;

        addSocial(values)

        this.setState({socialErr:""})
    }

    clearSocial = () => {

        let { addSocial } = this.props;

        addSocial("")
    }

    handleUserInput (e) {

        let { name } = this.props;

        const tname = e.target.name;
        const value = e.target.value;

        if (tname === name){
            let ll = value.replace(/\s\s+/g, ' ').replace(/^\s+/,'')

            this.setState({social: ll, socialErr:""});
        }

    }

    render() {

        let { label, name, domain, formValues } = this.props
        let { social, socialFieldActivate, socialErr } = this.state

        const link = _.get(formValues, name, false);

        function isDomain(name, url) {
            if(name === "linkedin") {
                return /^https:\/\/www.linkedin.com\/\b.*$/i.test(url)
            }
            if(name === "facebook") {
                return /^https:\/\/www.facebook.com\/\b.*$/i.test(url)
            }
            if(name === "twitter") {
                return /^https:\/\/twitter.com\/\b.*$/i.test(url)
            }
            if(name === "pinterest") {
                return /^https:\/\/www.pinterest.com\/\b.*$/i.test(url)
            }
            if(name === "youtube") {
                return /^https:\/\/www.youtube.com\/\b.*$/i.test(url)
            }
            if(name === "instagram") {
                return /^https:\/\/www.instagram.com\/\b.*$/i.test(url)
            }
            if(name === "telegram") {
                return /^https:\/\/t.me\/\b.*$/i.test(url)
            }
            if(name === "angel") {
                return /^https:\/\/angel.co\/\b.*$/i.test(url)
            }
            if(name === "glassdoor") {
                return /^https:\/\/www.glassdoor.com\/\b.*$/i.test(url)
            }
        }

        return (
            <div>
                <Row>   
                    <Col xs={12}>
                        <FormGroup className={name}>
                            <InputGroup>
                                <Input name={name} placeholder={socialFieldActivate ? 'https://'+domain+"/" : null}
                                    onFocus={() => this.setState({socialFieldActivate: true})}
                                    onBlur={(event) => { if(social === "") {this.setState({socialFieldActivate: false})} if(social && !isDomain(name, social)){this.setState({socialErr: "Enter "+label+" url"})}}}
                                    onChange={(event) => this.handleUserInput(event)} 
                                    value={social} type="text" autoComplete="off" 
                                    disabled={link ? true : false} />
                                <Label className={socialFieldActivate ? "active" : ""}>{label}</Label>
                                <InputGroupAddon addonType="append">
                                    <Button color="primary" disabled={social && !socialErr && isDomain(name, social) ? false : true}  onClick={link ? this.clearSocial : this.addSocial(social)}>{link ? "Unlink" : "Link"}</Button>
                                </InputGroupAddon>
                            </InputGroup>
                            {socialErr && <div className="text-danger"><small>{socialErr}</small></div>}
                        </FormGroup>
                    </Col>
                </Row>
            </div>
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

const mapDispatchToProps = (dispatch, ownProps) => {
 
    return {
        addSocial: (value) => {
            dispatch(change(ownProps.form, ownProps.name, value))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SocialNetworkField);