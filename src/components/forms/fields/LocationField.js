import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, getFormValues, Field } from 'redux-form';
import { FormGroup, Input, Label, InputGroup, InputGroupAddon } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';

import { MdDelete } from 'react-icons/md';

import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

class LocationField extends Component {

    constructor(props) {
        super(props) 
        
        this.state = {
            fieldActivate: _.get(props.formValues, props.input.name, false),
            errorMessage: "",
            autosuggestFocus:false,
            isGeocoding: false
        }
    }

    handleChange = () => {

        let { setCoords } = this.props;

        this.setState({ errorMessage: "" });

        setCoords("")
    }

    handleSelect = selected => {
        
        let { setCoords } = this.props;

        this.setState({ isGeocoding: true})

        geocodeByAddress(selected)
            .then(res => getLatLng(res[0]))
            .then((latLng) => {
                setCoords(latLng)
                this.setState({ isGeocoding: false });
            })
            .catch(error => {
                this.setState({ isGeocoding: false });
            });
    };

    handleError = (status, clearSuggestions) => {
        this.setState({ errorMessage: status }, () => {
            clearSuggestions();
        });
    };

    renderField = ({ autosuggestFocus, isGeocoding, remove, length, index, fieldActivate, errorMessage, input, label, meta: { touched, error } }) => (
        
        <div>
            <FormGroup className={index === length-1 ? "without-margin" : ""}>
                <PlacesAutocomplete
                    value={input.value}
                    onChange={(address) => {input.onChange(address);this.handleChange(address)}}
                    onSelect={(selected) => {input.onChange(selected);this.handleSelect(selected)}}
                    onError={this.handleError}
                    shouldFetchSuggestions={input.value.length > 2}>
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => {
                        return (
                            <div>
                                <InputGroup>
                                    <Input {...input} autoComplete="off"
                                        placeholder={fieldActivate ? label : null}
                                        className={classnames({invalid: (touched && error) ? true : false })}
                                        {...getInputProps()} onBlur={input.onBlur} onFocus={input.onFocus}
                                    />
                                    {index !==0 && <InputGroupAddon addonType="append" className="align-items-center">
                                        <MdDelete onClick={remove} className="close-location align-middle"/>
                                    </InputGroupAddon>}
                                </InputGroup>
                                <Label className={fieldActivate ? "active" : ""}>{label}</Label>
                                {isGeocoding && <div className="react-autosuggest__loading">Loading...</div>}
                                {suggestions.length > 0 && (
                                    <div className={autosuggestFocus ? "react-autosuggest__suggestions-container react-autosuggest__suggestions-container--open" : "react-autosuggest__suggestions-container" }>
                                        <ul className="react-autosuggest__suggestions-list">
                                            {loading && <div className="react-autosuggest__loading">Loading...</div>}
                                            {suggestions.map(suggestion => {
                                                const className = suggestion.active ? 'react-autosuggest__suggestion react-autosuggest__suggestion--highlighted' : 'react-autosuggest__suggestion';
                                                return (
                                                    <li
                                                        {...getSuggestionItemProps(suggestion, {className })}>
                                                        <span>{suggestion.description}</span>
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                            </div>
                        );
                    }}
                </PlacesAutocomplete>
                {errorMessage.length > 0 && <div className="text-danger"><small>{errorMessage}</small></div>}
                {touched && error && <div className="text-danger"><small>{error}</small></div>}
            </FormGroup>
        </div>
    )

    render() {

        let { fieldActivate, isGeocoding, autosuggestFocus, errorMessage } = this.state;
        let { index, length, input, label, remove } = this.props;
          
        return (
            <Field 
                length={length}
                index={index} 
                name={input.name} 
                type="text"
                component={this.renderField} 
                label={label}
                fieldActivate={fieldActivate}
                errorMessage={errorMessage}
                remove={remove}
                isGeocoding={isGeocoding}
                autosuggestFocus={autosuggestFocus}
                onFocus={() => {this.setState({fieldActivate: true, autosuggestFocus:true})}}
                onBlur={(event) => { this.setState({autosuggestFocus: false}); if(event.target.value === "") {this.setState({fieldActivate: false})}}}
            />
        )
    }
};


const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.meta.form)(state);

    return {formValues};
    
}

const mapDispatchToProps = (dispatch, ownProps) => {
 
    return {
        setCoords: (value) => {
            dispatch(change(ownProps.meta.form, `${ownProps.member}.coords`, value))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LocationField);