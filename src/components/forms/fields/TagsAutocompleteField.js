import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, getFormValues } from 'redux-form';
import { Badge, FormGroup, Input, Label, Button, InputGroup, InputGroupAddon } from 'reactstrap';
import _ from 'lodash';

import TagsInput from 'react-tagsinput'
import Autosuggest from 'react-autosuggest'

import { MdAdd, MdClose } from "react-icons/md";

class TagsAutocompleteField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            fieldActivate: false,
            showInput: false,
            suggestions: [],
            isLoading: false
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange (tag) {

        let { addTags, formValues, name } = this.props
        
        let lTag = _.last(tag)
        let tags = _.concat(_.get(formValues, name, []), lTag);

        addTags(tags)
    }

    onSuggestionsFetchRequested = ({value}) => {
        
        let { data, formValues, name } = this.props

        let tags = _.get(formValues, name, []);
        let inputValue = (value && value.trim().toLowerCase()) || ''
        let suggestions = _.filter(data, name => _.includes(name.toLowerCase(), inputValue) && !_.includes(tags,name))

        if (this.lastRequestId !== null) {
            clearTimeout(this.lastRequestId);
        }

        this.setState({isLoading: true});

        setTimeout(() => {
            this.setState({isLoading: false,suggestions: suggestions});
        }, 1000);  
    };

    onSuggestionsClearRequested = () => {
        this.setState({suggestions: []});
    };

    chooseTag = (tag) => {

        let { addTags, formValues, name } = this.props

        let newTags = _.concat(_.get(formValues, name, []), tag);
        addTags(newTags)
    }

    removeTag = (tag) => {

        let { addTags, formValues, name } = this.props

        let newTags = _.filter(_.get(formValues, name, []), name => !_.includes(name, tag));

        addTags(newTags)
    }

    render() {
    
        let { data, formValues, name, label } = this.props
        let { fieldActivate, showInput, suggestions, isLoading } = this.state

        let selectedTags = _.get(formValues, name, []);
        let tags = _.union(selectedTags, data)

        function defaultRenderTag (props) {

            let {tag, key, disabled, chooseTag, removeTag, onRemove, classNameRemove, getTagDisplayValue, className, ...other} = props

            return (
                <Badge className={_.includes(data, tag) && !_.includes(selectedTags, tag) ? className + " unactive" : className} onClick={_.includes(data, tag) && !_.includes(selectedTags, tag) ? () => chooseTag(tag) : null} key={key}>
                    <span {...other} className="align-middle">
                        {getTagDisplayValue(tag)}
                    </span>
                    {!disabled && _.includes(selectedTags, tag) && <MdClose className="close-icon align-middle" onClick={() => removeTag(tag)} onKeyDown={() => removeTag(tag)}/>}
                </Badge>
            )
        }

        function renderButtonComponent(inputProps) {

            let { changeInput } = inputProps

            return (
                <FormGroup>
                    <Button type="button" color="primary" onClick={changeInput} className="add-card-btn" size="lg">
                        <MdAdd className="add-icon align-middle" />
                        <span className="align-middle">Add</span>
                    </Button>
                </FormGroup>
            )
        }

        function renderInputComponent(inputProps) {

            let { addTag, isLoading, value, onChange, fieldActivate, ...other } = inputProps
            
            return (
                <FormGroup>
                    <InputGroup>
                        <Input type='text' onChange={onChange} value={value} {...other} autoComplete="off"/>
                        <Label className={fieldActivate ? "active" : null}>{label}</Label>
                        <InputGroupAddon addonType="append">
                            <Button color="primary" disabled={isLoading ? true : false} onClick={() => addTag(value)}>Add</Button>
                        </InputGroupAddon>
                    </InputGroup>
                </FormGroup>
            )
        }

        function defaultRenderLayout (tagComponents, inputComponent) {
            return (
                <div>
                    <div>
                        {tagComponents}
                    </div> 
                    <div className={name === "perksandbenefits.developmentgrowth" ? "add-perks-and-benefits-input-container align-middle without-padding" : "add-perks-and-benefits-input-container align-middle"}>
                        {inputComponent}
                    </div>
                </div>
            )
        }

        function autocompleteRenderInput ({addTag, onSuggestionsClearRequested, onSuggestionsFetchRequested, ...props}) {

            const handleOnChange = (e, {newValue, method}) => {
                if (method === 'enter') {
                    e.preventDefault()
                } else {
                    props.onChange(e)
                }
            }

            return (
                
                <Autosuggest
                    ref={props.ref}
                    suggestions={props.suggestions}
                    shouldRenderSuggestions={(value) => value && value.trim().length > 0}
                    getSuggestionValue={(suggestion) => suggestion}
                    renderSuggestion={(suggestion) => <span>{suggestion}</span>}
                    inputProps={{...props, onChange: handleOnChange, addTag: addTag}}
                    onSuggestionSelected={(e, {suggestion}) => {
                        addTag(suggestion);
                    }}
                    onSuggestionsClearRequested={onSuggestionsClearRequested}
                    onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                    renderInputComponent={renderInputComponent}
                />
            )
        }
      
        return (
            
            <TagsInput 
                renderLayout={defaultRenderLayout}
                renderTag={defaultRenderTag}
                renderInput={showInput ? autocompleteRenderInput : renderButtonComponent} 
                value={tags}
                tagProps={
                    {
                        className: "perks-and-benefits-card",
                        chooseTag: this.chooseTag,
                        removeTag: this.removeTag
                    }
                } 
                inputProps={
                    showInput ? 
                        {
                            onSuggestionsFetchRequested: this.onSuggestionsFetchRequested,
                            onSuggestionsClearRequested: this.onSuggestionsClearRequested,
                            isLoading: isLoading,
                            suggestions: suggestions,
                            fieldActivate:fieldActivate,
                            placeholder: fieldActivate ? label : null,
                            onFocus:() => {this.setState({fieldActivate: true})},
                            onBlur:(event) => {if(event.target.value === "") {this.setState({fieldActivate: false, showInput:!showInput})}}
                        }
                        :
                        {
                            changeInput: () => {this.setState({showInput:!showInput})}
                        }
                }
                onChange={this.handleChange}/>
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

const mapDispatchToProps = (dispatch, ownProps) => {
 
    return {
        addTags: (value) => {
            dispatch(change(ownProps.form, ownProps.name, value))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TagsAutocompleteField);