import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, getFormValues } from 'redux-form';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import { Row, Col, ButtonToolbar, Button } from 'reactstrap';
import _ from 'lodash';

import { DotLoader } from 'react-spinners';

import upload from '../../../assets/upload.svg'
import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

import { MdClear } from "react-icons/md";

class OfficeImagesUploadField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            dropError: "",
            isLoading: false,
            page:0
        }

        this.dropzoneRef = {};

        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }

    nextPage() {
        let { page } = this.state

        this.setState({page: page+1})
    }

    previousPage() {
        let { page } = this.state

        this.setState({page: page-1})
    }

    onDropAccepted = (file) => {

        let { addPhotos, name, formValues } = this.props;

        this.setState({dropError: "", isLoading: true});

        const loadImageOptions = { canvas: true }

        loadImage.parseMetaData(file[0], (data) => {

            if (data.exif && data.exif.get('Orientation')) {
                loadImageOptions.orientation = data.exif.get('Orientation')
            }

            loadImage(file[0], (canvas) => {
                file[0].preview = canvas.toDataURL(file[0].type)

                let preview = _.map(file, 'preview');
                let newPhotos = _.concat(preview, _.get(formValues, name, []));
                let self = this;

                setTimeout(() => {
                    addPhotos(newPhotos);
        
                    self.setState({page:0,isLoading: false})
                }, 1000);

            }, loadImageOptions)
        })         
        
    }
    
    onDropRejected = () => {

        this.setState({dropError: "Maximum image size is 5 MB. Formats: png, jpeg, jpg."});
    }

    clearImage = (index) => () => {

        let { addPhotos, name, formValues } = this.props;
        let { page } = this.state

        this.setState({dropError: ""});

        let newArray = _.reduce(_.get(formValues, name, []), function(result, value, key) {

            if(key !== index) {
                result.push(value)
            }
            return result;
        }, [])

        if(newArray.length === 0){
            addPhotos("")
        } else {
            addPhotos(newArray)
        }
        
        if(_.chunk(newArray, 3).length === page && page !== 0) {
            this.setState({page:page-1});
        }
    }

    render() {

        let { dropError, isLoading, page } = this.state
        let { formValues, name, label } = this.props;

        const photos = _.get(formValues, name, []);

        return (
            <div>
                <ButtonToolbar className={(!photos || !Array.isArray(photos) || photos.length === 0) ? "justify-content-start align-items-center" : "justify-content-between align-items-center"}>
                    <div className="our-office-label">Our office</div>
                    {(!photos || !Array.isArray(photos) || photos.length === 0 ) ? null : <div className="arrow-btns">
                        <Button type="button" disabled={(!photos || !Array.isArray(photos) || _.chunk(photos, 3).length === 1 || page === 0) ? true : false} color="primary" size="lg" onClick={this.previousPage}>
                            <img className="back-icon align-middle" src={back} alt="back"/>
                        </Button>
                        <Button type="button" color="primary" size="lg" onClick={this.nextPage} disabled={(!photos || !Array.isArray(photos) || _.chunk(photos, 3).length === 1 || page+1 === _.chunk(photos, 3).length ) ? true : false} >
                            <img className="next-icon align-middle" src={next} alt="next"/>
                        </Button>
                    </div>}
                </ButtonToolbar>
                <Row>
                    <Col xs={12} md={3}>
                        <div className="drop-zone-container">
                            <Dropzone
                                name={name}
                                onDropAccepted={this.onDropAccepted}
                                onDropRejected={this.onDropRejected}
                                ref={(node) => { this.dropzoneRef = node; }}
                                maxSize={5242880}
                                multiple={false}
                                accept="image/png,image/jpeg"
                                className="drop-zone d-table">
                                {isLoading ? <div className="loading-image d-table-cell align-middle">
                                    <div className="loading-container text-center">
                                        <DotLoader color={'#8a8a8f'} size={36} loading={true}/>
                                    </div>
                                </div> 
                                    : <div className="upload-image d-table-cell align-middle">
                                        <img className="upload-icon" src={upload} alt="upload" /><br/>
                                        <span>Upload {label}</span>
                                    </div>
                                }
                            </Dropzone>
                            {dropError && <div className="text-danger"><small>{dropError}</small></div>}
                        </div>
                    </Col>
                    {photos && Array.isArray(photos) && _.chunk(photos, 3)[page] &&
                    _.chunk(photos, 3)[page].map((image, index) => {
                        return <Col xs={12} md={3} key={index}>
                            <div className="images-array d-table">
                                <div className="d-table-cell align-middle">
                                    <img className="image-preview align-middle" src={image} alt="response"/>
                                </div>
                                {!isLoading && <MdClear onClick={this.clearImage(index+((page)*3))} className="upload-images-delete"/>}
                            </div>
                        </Col>
                    })
                    }
                </Row>
            </div>
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

const mapDispatchToProps = (dispatch, ownProps) => {
 
    return {
        addPhotos: (value) => {
            dispatch(change(ownProps.form, ownProps.name, value))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OfficeImagesUploadField);