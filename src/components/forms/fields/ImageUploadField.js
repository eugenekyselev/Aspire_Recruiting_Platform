import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, getFormValues } from 'redux-form';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import _ from 'lodash';
import classnames from 'classnames';

import { MdClear } from "react-icons/md";

import { DotLoader } from 'react-spinners';

import upload from '../../../assets/upload.svg'

class ImageUploadField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            dropError: "",
            isLoading: false
        }

        this.dropzoneRef = {};
    }

    onDropAccepted = (file) => {

        let { addPhoto } = this.props;

        this.setState({dropError: "", isLoading: true});

        const loadImageOptions = { canvas: true }

        loadImage.parseMetaData(file[0], (data) => {

            if (data.exif && data.exif.get('Orientation')) {
                loadImageOptions.orientation = data.exif.get('Orientation')
            }

            loadImage(file[0], (canvas) => {
                file[0].preview = canvas.toDataURL(file[0].type)

                let preview = _.map(file, 'preview');
                let self = this;

                setTimeout(() => {
                    addPhoto(preview);
        
                    self.setState({isLoading: false})
                }, 1000);

            }, loadImageOptions)
        }) 

    }
    
    onDropRejected = () => {

        this.setState({dropError: "Maximum image size is 5 MB. Formats: png, jpeg, jpg."});
    }

    clearImage = () => {

        let { addPhoto } = this.props;

        this.setState({dropError: ""});

        addPhoto("");
    }

    render() {

        let { dropError, isLoading } = this.state
        let { formValues, name, label } = this.props;

        const photo = _.get(formValues, name, "");

        return (
            <div className="drop-zone-container">
                <Dropzone
                    name={name}
                    onDropAccepted={this.onDropAccepted}
                    onDropRejected={this.onDropRejected}
                    ref={(node) => { this.dropzoneRef = node; }}
                    maxSize={5242880}
                    multiple={false}
                    accept="image/png,image/jpeg"
                    className={classnames(photo && Array.isArray(photo) ? "active" : null, "drop-zone d-table")}>
                    {isLoading ? <div className="loading-image d-table-cell align-middle">
                        <div className="loading-container text-center">
                            <DotLoader color={'#8a8a8f'} size={36} loading={true}/>
                        </div>
                    </div> : photo && Array.isArray(photo) ? 
                        <div className="replace-image d-table-cell align-bottom">
                            <img className="upload-icon" src={upload} alt="upload" /><br/>
                            <span>Replace {label}</span>
                        </div>
                        :
                        <div className="upload-image d-table-cell align-middle">
                            <img className="upload-icon" src={upload} alt="upload" /><br/>
                            <span>Upload {label}</span>
                        </div>}
                    {photo && Array.isArray(photo) && 
                    <div className="d-table-cell align-middle">
                        <img className="image-preview align-middle" src={photo} alt="response"/>
                    </div>
                    }
                </Dropzone>
                {photo && !isLoading && Array.isArray(photo) && 
                <MdClear onClick={this.clearImage} className="clear-uploaded-image"/>   
                }
                {dropError && <div className="text-danger"><small>{dropError}</small></div>}
            </div>
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

const mapDispatchToProps = (dispatch, ownProps) => {
 
    return {
        addPhoto: (value) => {
            dispatch(change(ownProps.form, ownProps.name, value))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImageUploadField);