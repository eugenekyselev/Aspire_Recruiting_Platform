import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, getFormValues } from 'redux-form';
import { Button/*, ButtonToolbar */} from 'reactstrap';
import _ from 'lodash';

import { MdAdd } from 'react-icons/md';

import LocationField from './LocationField'

class LocationsArray extends Component {

    render() {
    
        let {formValues, fields, meta: { error }} = this.props
    
        return (
            <div>
                {
                    fields.map((member, index) => (
                        <div key={index}>
                            <Field
                                length={fields.length}
                                index={index}
                                member={member}
                                remove={() => fields.remove(index)}
                                locForm="companyProfileForm"
                                name={`${member}.address`}
                                locName={`${member}.address`}
                                component={LocationField}
                                label={index === 0 ? "Headquarters" : "Office #"+index}/>
                            {/*index !== 0 && !addLocation &&
                            <ButtonToolbar className="justify-content-start wait-location-btns">
                                <Button type="button" color="primary" className="add-location-wait-btn" onClick={this.waitForAdding}>
                                    Add location
                                </Button>
                                <Button type="button" color="primary" outline className="cancel-wait-btn" onClick={this.cancelAdding(index)}>
                                    Cancel
                                </Button>
                            </ButtonToolbar>*/}
                        </div>
                    ))}
                <Button type="button" disabled={_.get(formValues, `locations[${fields.length-1}].coords`, "") ? false : true} color="primary" className="add-location-btn" size="lg"  onClick={() => fields.push({address:"",coords:""})}>
                    <MdAdd className="add-icon align-middle" />
                    <span className="align-middle">Add location</span>
                </Button>
                { error && <div className="text-danger"><small>{error}</small></div>}
            </div>
            
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
    
}

export default connect(mapStateToProps,{})(LocationsArray);