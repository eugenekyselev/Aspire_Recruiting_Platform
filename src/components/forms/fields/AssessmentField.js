import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, getFormValues } from 'redux-form';
import { FormGroup, Input, Label, Button, ButtonToolbar, Row, Col, Table, Progress } from 'reactstrap';

import { toast } from 'react-toastify';
import copy from 'copy-to-clipboard';

import _ from 'lodash';

import { Radar } from 'react-chartjs-2';

class AssessmentField extends Component {

    renderField = ({ input, label, type }) => (
        <FormGroup className={input.name} check>
            <Label className="checkbox-container">{label}
                <Input {...input} type={type} />
                <span className="checkmark"></span>
            </Label>
        </FormGroup>
    )

    copyLink = () => {

        let { shareLink } = this.props
        
        copy(shareLink);
        toast("Copied!")
    }

    shareByEmail = () => {

        let { shareLink } = this.props

        window.open('mailto:test@example.com?body='+shareLink, '_self');
    }

    render() {
    
        let { shareLink, assessmentResult:{culture,personalities} } = this.props

        let currentSum = _.reduce(culture, function(sum, n) {
            return sum + n.data[0].current;
        }, 0);

        let preferredSum = _.reduce(culture, function(sum, n) {
            return sum + n.data[1].preferred;
        }, 0);

        let allLabels = _.reduce(culture, function(labels, n) {
            labels.push(n.type.toUpperCase());
            return labels
        }, []);
        
        let allCurrentData = _.reduce(culture, function(data, n) {
            data.push(n.data[0].current); 
            return data
        }, []);

        let allPreferredData = _.reduce(culture, function(data, n) {
            data.push(n.data[1].preferred); 
            return data
        }, []);

        const capitalize = (str) => {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }

        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} md={12} lg={6}>
                        <div className="assessment-section">Your Culture</div>
                        <div className="assessment-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th>Culture</th>
                                    <th className="current">Current</th>
                                    <th className="preferred">Preferred</th>
                                </tr>
                            </thead>
                            <tbody>
                                {culture.map((item, index) =>
                                    (index <= 3) ? 
                                        <tr key={index}>
                                            <td>{capitalize(item.type)}</td>
                                            <td>{item.data[0].current}</td>
                                            <td>{item.data[1].preferred}</td>
                                        </tr>
                                        : 
                                        null
                                )}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th>{currentSum}</th>
                                    <th>{preferredSum}</th>
                                </tr>
                            </tfoot>
                        </Table>
                        <div className="show-company-culture">
                            <Field 
                                name="currentcompanyculture"
                                type="checkbox"
                                component={this.renderField}
                                label="Show current company culture type in public profile"
                            />
                            <Field 
                                name="preferredcompanyculture"
                                type="checkbox"
                                component={this.renderField}
                                label="Show preferred company culture type in public profile"
                            />
                        </div>
                    </Col>
                    <Col xs={12} md={12} lg={6} style={{paddingLeft:"0px", paddingRight:"0px"}}>
                        <Radar height={320} options={{
                            scale:{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 5
                                },
                                angleLines:{
                                    color: "#c8c7cc"
                                },
                                pointLabels:{
                                    fontColor:"#c8c7cc",
                                    fontSize:12
                                }
                            },
                            legend: {
                                display: false
                            },
                            layout: {
                                padding: {
                                    left: 8,
                                    right: 8,
                                    top: 8,
                                    bottom: 8
                                },
                            },
                            responsive: true
                        }} data={{
                            labels: allLabels,
                            datasets: [
                                {
                                    label: 'Current',
                                    backgroundColor: 'rgba(0, 122, 255, 0.3)',
                                    borderColor: '#007aff',
                                    pointBackgroundColor: '#007aff',
                                    pointBorderColor: '#fff',
                                    pointHoverBackgroundColor: '#fff',
                                    pointHoverBorderColor: '#007aff',
                                    data: allCurrentData
                                },
                                {
                                    label: 'Preferred',
                                    backgroundColor: 'rgba(255, 45, 85, 0.3)',
                                    borderColor: '#ff2d55',
                                    pointBackgroundColor: '#ff2d55',
                                    pointBorderColor: '#fff',
                                    pointHoverBackgroundColor: '#fff',
                                    pointHoverBorderColor: '#ff2d55',
                                    data: allPreferredData
                                }
                            ]
                        }} />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <div className="assessment-section">Your Personalities</div>
                        <div className="assessment-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            <br/><br/>Also, if you want to have more relevant result, share the link bellow with your employees and get accurate result.</div>
                        <FormGroup>
                            <Input placeholder={shareLink ? "Survey link" : null} value={shareLink} type="text" autoComplete="off" readOnly/>
                            <Label className={shareLink ? "active" : ""}>Survey link</Label>
                        </FormGroup>
                        <ButtonToolbar className="link-btns justify-content-start align-items-center">
                            <Button type="button" onClick={this.copyLink} color="primary">Copy link</Button>
                            <div className="or">or</div>
                            <Button type="button" onClick={this.shareByEmail} color="primary">Share by Email</Button>
                        </ButtonToolbar>
                    </Col>
                    <Col className="progressbar-container" xs={12} md={6}>
                        <Row>
                            <Col xs={6}>
                                <div className="d-flex justify-content-start align-items-center">
                                    <div className="num-of-people">37</div>
                                    <div className="desc-of-people">
                                        <div className="people-passed">people passed</div>
                                        <div className="people-when">last 5 days</div>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={6}>
                                <div className="d-flex justify-content-start align-items-center">
                                    <div className="num-of-people">145</div>
                                    <div className="desc-of-people">
                                        <div className="people-passed">people passed</div>
                                        <div className="people-when">last month</div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        {_.orderBy(personalities, ['value'], ['desc']).map((item, index) => 
                            (index <= 3) ? 
                                <React.Fragment key={index}>
                                    <div className="progressbar-label">{capitalize(item.type)}</div>
                                    <Row className={index === 3 ? "progressbar-element without-margin align-items-center" : "progressbar-element align-items-center"}>
                                        <Col xs={9} className="mx-auto" >
                                            <Progress value={item.value} max="28"/>
                                        </Col>
                                        <Col xs={3} className="mx-auto" >
                                            <div className="progressbar-count">{item.value}%</div>
                                        </Col>
                                    </Row>
                                </React.Fragment>
                                :
                                null
                        )}
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(mapStateToProps, {})(AssessmentField);