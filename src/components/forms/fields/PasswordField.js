import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFormValues, Field } from 'redux-form';
import { FormGroup, Input, Label, InputGroup, InputGroupAddon } from 'reactstrap';
import { MdRemoveRedEye } from 'react-icons/md';
import classnames from 'classnames';
import _ from 'lodash';

class PasswordField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            type: "password",
            fieldActivate: _.get(props.formValues, props.name, false)
        }

        this.showHide = this.showHide.bind(this);
    }

    showHide = e => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })  
    }

    renderField= ({ fieldActivate, input, label, type, meta: { touched, error } }) => (
        <FormGroup className={input.name}>
            <InputGroup>
                <Input placeholder={fieldActivate ? label : null} {...input} className={classnames({invalid: (touched && error) ? true : false })} type={type} autoComplete="off" required/>
                <Label className={fieldActivate ? "active" : ""}>{label}</Label>
                {input.value && <InputGroupAddon addonType="append" className="align-items-center">
                    <MdRemoveRedEye onClick={this.showHide} className={classnames({active: this.state.type === 'input' ? "active" : null}, "eye-icon align-middle")}/>
                </InputGroupAddon>}
            </InputGroup>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    render() {

  	    let { type, fieldActivate } = this.state
  	    let { name, label } = this.props;
    
        return (
            <Field 
                name={name} 
                type={type} 
                component={this.renderField} 
                label={label}
                fieldActivate={fieldActivate}
                onFocus={() => this.setState({fieldActivate: true})}
                onBlur={(event) => { if(event.target.value === "") {this.setState({fieldActivate: false})}}}
            />
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(mapStateToProps,{})(PasswordField);