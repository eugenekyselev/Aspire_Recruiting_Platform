import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFormValues, Field } from 'redux-form';
import { FormGroup, Input, Label } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';

class SelectField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            fieldActivate: _.get(props.formValues, props.name, false)
        }
    }

   
    renderField = ({ options, fieldActivate, input, label, type, meta: { touched, error } }) => (
        <FormGroup className={input.name}>
            <Input {...input} className={classnames({invalid: (touched && error) ? true : false })} type={type} autoComplete="off">
                <option value=""/>
                {options && options.map((option,i) => (<option key={i} value={option}>{option}</option>))}
            </Input>
            <Label className={fieldActivate ? "active" : ""}>{label}</Label>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    render() {

  	    let { fieldActivate } = this.state
  	    let { name, label, options } = this.props;
    
        return (
            <Field 
                name={name} 
                type="select" 
                options={options}
                component={this.renderField} 
                label={label}
                fieldActivate={fieldActivate}
                onChange={(event) => {if(event.target.value === "") {this.setState({fieldActivate: false})} else {this.setState({fieldActivate: true})}}}
            />
        )
    }
};

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(mapStateToProps,{})(SelectField);