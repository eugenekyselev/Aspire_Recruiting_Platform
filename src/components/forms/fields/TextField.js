import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFormValues, Field } from 'redux-form';
import { FormGroup, Input, Label } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';

class TextField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            fieldActivate: _.get(props.formValues, props.name, false)
        }

        this.trim = this.trim.bind(this);
    }
   
    renderField = ({ fieldActivate, input, label, type, meta: { touched, error } }) => (
        <FormGroup className={input.name}>
            <Input placeholder={fieldActivate ? label : null} {...input} className={classnames({invalid: (touched && error) ? true : false })} type={type} autoComplete="off"/>
            <Label className={fieldActivate ? "active" : ""}>{label}</Label>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    trim = value => (value && value.replace(/\s\s+/g, ' ').replace(/^\s+/,''))

    render() {

  	    let { fieldActivate } = this.state
  	    let { name, label, shouldTrim } = this.props;
    
        return (
            <Field 
                name={name} 
                type="text"
                component={this.renderField}
                label={label}
                normalize={shouldTrim && this.trim} 
                fieldActivate={fieldActivate}
                onFocus={() => this.setState({fieldActivate: true})}
                onBlur={(event) => { if(event.target.value === "") {this.setState({fieldActivate: false})}}}
            />
        )
    }
};


const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(mapStateToProps, {})(TextField);