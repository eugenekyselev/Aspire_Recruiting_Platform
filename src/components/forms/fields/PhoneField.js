import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFormValues, Field } from 'redux-form';
import { FormGroup, Input, Label } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';

class PhoneField extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            fieldActivate: _.get(props.formValues, props.name, false)
        }

    }
   
    renderField = ({ fieldActivate, input, label, type, meta: { touched, error } }) => (
        <FormGroup className={input.name}>
            <Input placeholder={fieldActivate ? "+" : null} {...input} className={classnames({invalid: (touched && error) ? true : false })} type={type} onPaste={(evt) => evt.preventDefault()} onKeyPress={(evt) => {if (evt.key && evt.key !== "Backspace" && evt.key !== "0" && evt.key !== "1" && evt.key !== "2" && evt.key !== "3" && evt.key !== "4" && evt.key !== "5" && evt.key !== "6" && evt.key !== "7" && evt.key !== "8" && evt.key !== "9" && evt.key !== "+" ) {evt.preventDefault();}}} autoComplete="off"/>
            <Label className={fieldActivate ? "active" : ""}>{label}</Label>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    render() {

  	    let { fieldActivate } = this.state
  	    let { name, label } = this.props;
    
        return (
            <Field 
                name={name} 
                type="text"
                component={this.renderField}
                label={label}
                fieldActivate={fieldActivate}
                onFocus={() => this.setState({fieldActivate: true})}
                onBlur={(event) => { if(event.target.value === "") {this.setState({fieldActivate: false})}}}
            />
        )
    }
};


const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(mapStateToProps, {})(PhoneField);