import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, Alert, ButtonGroup, ButtonToolbar } from 'reactstrap';
import _ from 'lodash';

import { resetPassword, clearResetPassword } from '../../../actions'

import EmailField from '../fields/EmailField'

import validate from '../validations/ResetPasswordFormValidation'

class ResetPasswordForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this)
    }

    submit = values => {

        let { resetPassword } = this.props;

        resetPassword(values.email, "resetPasswordForm");
    }

    componentWillUnmount(){

        let { clearResetPassword} = this.props;

        clearResetPassword();
    }

    render() {

        let { alert, handleSubmit, pristine, submitting, invalid } = this.props;

        return (
            <div>
                {alert && !_.isEmpty(alert) ? !alert.error ? <Alert color="success">{alert.message}</Alert> : <Alert color="danger">{alert.message}</Alert> : null}
                <h3>Reset Password</h3>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <EmailField form="resetPasswordForm" name="email" label="Email" />
                    <ButtonToolbar className="justify-content-between align-items-center">
                        <ButtonGroup>
                            <Button type="submit" disabled={pristine || submitting || invalid} color="primary" size="lg">Reset</Button>
                        </ButtonGroup>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        alert: state.resetPassword.alert
    }
}

ResetPasswordForm = connect(
    mapStateToProps,
    {
        resetPassword, clearResetPassword
    }
)(ResetPasswordForm);

export default reduxForm({
    initialValues: {
        
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'resetPasswordForm',
    validate
})(ResetPasswordForm)