import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FieldArray, reduxForm } from 'redux-form';
import { Col, Row, Container, Nav, NavItem, Button, Form, Alert, Card, ButtonToolbar } from 'reactstrap';

import { Link, Events, scrollSpy } from 'react-scroll'

import Sticky from 'react-sticky-el';

import TextField from '../fields/TextField'
import SelectField from '../fields/SelectField'
import PhoneField from '../fields/PhoneField'
import ImageUploadField from '../fields/ImageUploadField'
import OfficeImagesUploadField from '../fields/OfficeImagesUploadField'
import TeamUploadField from '../fields/TeamUploadField'
import LocationsArray from '../fields/LocationsArray'
import TextareaField from '../fields/TextareaField'
import TagsAutocompleteField from '../fields/TagsAutocompleteField'
import AssessmentField from '../fields/AssessmentField'
import RepresentativeField from '../fields/RepresentativeField'
import SocialNetworkField from '../fields/SocialNetworkField'

import CompanyProfileView from './CompanyProfileView'

import validate from '../validations/ProfileFormValidation'

import { changeProfilePage } from '../../../actions'

class CompanyProfileForm extends Component {

    componentDidMount () {

        Events.scrollEvent.register('begin', function () {})
        Events.scrollEvent.register('end', function () {})

        scrollSpy.update()
    }

    componentWillUnmount () {
        Events.scrollEvent.remove('begin')
        Events.scrollEvent.remove('end')
    }

    render() {

        let { page, changeProfilePage, reset, error, handleSubmit, pristine, submitting, invalid } = this.props;

        return (
            page === "form" ?
                <Container>
                    <Row>
                        <Col className="profile-sidebar" xs={12} md={3}>
                            <Nav pills vertical className="sticky-top">
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="company-information"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Company Information
                                    </Link>                                  
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="locations"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Locations
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="perks-and-benefits"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Perks and Benefits
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="assessment"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Assessment
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="representative"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Representative
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="nav-link"
                                        activeClass="active"
                                        to="social-networks"
                                        spy={true}
                                        smooth={true}
                                        offset={-200}
                                        duration={500}>
                                    Social Networks
                                    </Link>
                                </NavItem>
                            </Nav>
                        </Col>
                        <Col className="profile-form-section" xs={12} md={9}>
                            {error && <Alert color="danger">{error}</Alert>}
                            <Form onSubmit={handleSubmit} className="profile-form">
                                <Card id="company-information">
                                    <ButtonToolbar className="justify-content-between align-items-center">
                                        <div className="company-information-title">Company Information</div>
                                        <Button type="button" disabled={pristine || submitting || invalid} className="view-company-page-btn" color="link" onClick={() => changeProfilePage("view")}>View company page</Button>
                                    </ButtonToolbar>
                                    <Row>
                                        <Col xs={12} md={3}>
                                            <ImageUploadField form="companyProfileForm" name="uploadlogo" label="logo" />
                                        </Col>
                                        <Col xs={12} md={9}>
                                            <ImageUploadField form="companyProfileForm" name="uploadcover" label="cover" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} md={6}>
                                            <TextField form="companyProfileForm" name="companyname" label="Company name" shouldTrim={true} />
                                        </Col>
                                        <Col xs={12} md={6}>
                                            <SelectField form="companyProfileForm" options={["Public company", "Private company"]} name="companytype" label="Company type" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} md={6}>
                                            <SelectField form="companyProfileForm" options={["Accounting","Airlines/Aviation","Marketing and Advertising","Military","IT"]} name="industry" label="Industry" />
                                        </Col>
                                        <Col xs={12} md={6}>
                                            <SelectField form="companyProfileForm" options={["1-5","5-20","20-100",'100-500','500-1000','1000-10000']} name="numberofemployees" label="Number of employees" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} md={6}>
                                            <TextField form="companyProfileForm" name="website" label="Website" />
                                        </Col>
                                        <Col xs={12} md={6}>
                                            <PhoneField form="companyProfileForm" name="phone" label="Phone" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <TextareaField form="companyProfileForm" name="whatwedo" label="What we do" shouldTrim={true} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <TextareaField form="companyProfileForm" name="whywedowhatwedo" label="Why we do it" shouldTrim={true} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <TextareaField form="companyProfileForm" name="weareproudof" label="We are proud of" shouldTrim={true} />
                                        </Col>
                                    </Row>
                                </Card>
                                <Card id="locations">
                                    <div className="location-title">Locations</div>
                                    <Row>
                                        <Col xs={12}>
                                            <FieldArray form="companyProfileForm" name="locations" component={LocationsArray} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <OfficeImagesUploadField form="companyProfileForm" name="ouroffice" label="photos" />
                                            <TeamUploadField form="companyProfileForm" name="ourteam" label="team" />
                                        </Col>
                                    </Row> 
                                </Card>
                                <Card id="perks-and-benefits">
                                    <div className="perks-and-benefits-title">Perks and Benefits</div>

                                    <div className="perks-and-benefits-section">Work-Life Balance</div>
                                    <div className="perks-and-benefits-desc">We make sure our team has a good work-life balance</div>
                        
                                    <TagsAutocompleteField form="companyProfileForm" name="perksandbenefits.worklifebalance" label="Work-Life Balance" 
                                        data={['flexible working hours','children creche','on-site childcare','dog friendly office','working from home opportunities','maternity / paternity benefits','retirement plan','personal days','sabbatical']}/>

                                    <div className="perks-and-benefits-section">Compensation</div>
                                    <div className="perks-and-benefits-desc">We provide plenty of opportunities to ensure financial freedom</div>
                        
                                    <TagsAutocompleteField form="companyProfileForm" name="perksandbenefits.compensation" label="Compensation" 
                                        data={['individual / company performance based bonus scheme','affiliate scheme','clear career development plan','promotion opportunities','overtime pay']}/>

                                    <div className="perks-and-benefits-section">Environment & Community</div>
                                    <div className="perks-and-benefits-desc">We are passionate about making a positive difference to our community and the world</div>
                        
                                    <TagsAutocompleteField form="companyProfileForm" name="perksandbenefits.environmentcommunity" label="Environment & Community" 
                                        data={['active apprentice program','engagement with selected charities','environmentally friendly programs','accommodation support','diversity programs']}/>

                                    <div className="perks-and-benefits-section">Wealth & Health</div>
                                    <div className="perks-and-benefits-desc">We invest in our team’s healthy lifestyle</div>
                        
                                    <TagsAutocompleteField form="companyProfileForm" name="perksandbenefits.wealthhealth" label="Wealth & Health" 
                                        data={['gym membership','free fruit in the office','office yoga','outdoor working space','medical benefits','access to life coach','health insurance','dental insurance','company products / discounts']}/>

                                    <div className="perks-and-benefits-section">Development & Growth</div>
                                    <div className="perks-and-benefits-desc">We ensure plenty of opportunities to develop our team’s professional and personal skills</div>
                        
                                    <TagsAutocompleteField form="companyProfileForm" name="perksandbenefits.developmentgrowth" label="Development & Growth" 
                                        data={['on-going training programs','paid or subsidised professional courses','one-to-one coaching and mentoring','tuition assistance / reimbursement']}/>
                                </Card>
                                <Card id="assessment">
                                    <div className="assessment-title">Assessment</div>

                                    <AssessmentField form="companyProfileForm" shareLink="https://aspire.io/awe7fwew6wefw7otyty8fg"
                                        assessmentResult={{
                                            culture:[{type:"hierarchy", data:[{current:26.32},{preferred:29.41}]},
                                                {type:"clan", data:[{current:25.27},{preferred:25.27}]},
                                                {type:"adhocracy", data:[{current:29.41},{preferred:26.32}]},
                                                {type:"market", data:[{current:18.99},{preferred:18.99}]}],
                                            personalities:[{type:"hierarchy",value:26},
                                                {type:"clan",value:24},
                                                {type:"adhocracy",value:28},
                                                {type:"market",value:22}]}}/>
                                </Card>
                                <Card id="representative">
                                    <div className="representative-title">Representative</div>
                       
                                    <RepresentativeField form="companyProfileForm"  />
                                </Card>
                                <Card id="social-networks">
                                    <div className="social-networks-title">Social Networks</div>
                        
                                    <SocialNetworkField form="companyProfileForm" name="linkedin" domain="www.linkedin.com" label="Linkedin" />
                                    <SocialNetworkField form="companyProfileForm" name="facebook" domain="www.facebook.com" label="Facebook" />
                                    <SocialNetworkField form="companyProfileForm" name="twitter" domain="twitter.com" label="Twitter" />
                                    <SocialNetworkField form="companyProfileForm" name="pinterest" domain="www.pinterest.com" label="Pinterest" />
                                    <SocialNetworkField form="companyProfileForm" name="youtube" domain="www.youtube.com" label="YouTube" />
                                    <SocialNetworkField form="companyProfileForm" name="instagram" domain="www.instagram.com" label="Instagram" />
                                    <SocialNetworkField form="companyProfileForm" name="telegram" domain="t.me" label="Telegram" />
                                    <SocialNetworkField form="companyProfileForm" name="angel" domain="angel.co" label="Angel List" />
                                    <SocialNetworkField form="companyProfileForm" name="glassdoor" domain="www.glassdoor.com" label="Glassdoor" />
                                </Card>
                    
                                <Sticky stickyStyle={{minHeight: "80px", zIndex: "5"}} mode="bottom" boundaryElement=".profile-form">
                                    {!(pristine || submitting || invalid) && 
                                    <div className="profile-submit">
                                        <ButtonToolbar className="justify-content-center align-items-center">
                                            <Button type="button" color="primary" outline size="lg" className="submit-btn" onClick={reset}>Cancel</Button>{" "}
                                            <Button type="submit" color="primary" size="lg" className="submit-btn">Save</Button>
                                        </ButtonToolbar> 
                                    </div>}
                                </Sticky>
                            </Form>
                        </Col>
                    </Row>
                </Container>
                :
                <CompanyProfileView form="companyProfileForm" submit={handleSubmit} changePage={() => changeProfilePage("form")} pristine={pristine} submitting={submitting} invalid={invalid}
                    culture={[{type:"hierarchy", data:[{current:26.32},{preferred:29.41}]},
                        {type:"clan", data:[{current:25.27},{preferred:25.27}]},
                        {type:"adhocracy", data:[{current:29.41},{preferred:26.32}]},
                        {type:"market", data:[{current:18.99},{preferred:18.99}]}]}/>
        );
    }
};

const mapStateToProps = (state, ownProps) => {

    return {
        page: state.profilePage.page
    };
}

CompanyProfileForm = connect(
    mapStateToProps,
    {
        changeProfilePage
    }
)(CompanyProfileForm);

export default reduxForm({
    initialValues: {
        companyname: "Google Inc",
        locations: [
            {
                address: "",
                coords: ""
            }
        ],
        members: [],
        perksandbenefits: {
            worklifebalance:[],
            compensation:[],
            environmentcommunity:[],
            wealthhealth:[],
            developmentgrowth:[]
        },
        currentcompanyculture: true,
        preferredcompanyculture: true,
        representative:{}
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'companyProfileForm',
    validate
})(CompanyProfileForm)