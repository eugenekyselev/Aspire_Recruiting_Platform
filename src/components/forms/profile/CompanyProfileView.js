import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';
import _ from 'lodash';

import { Col, Row, Container, Media, Badge, Button, Table, Card, ButtonToolbar, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { FaLinkedinIn, FaFacebookF, FaTwitter, FaTelegram, FaPinterest, FaYoutube, FaInstagram, FaAngellist, FaGlobe } from 'react-icons/fa';
import { MdMoreVert, MdCropFree } from 'react-icons/md';

import { Doughnut } from 'react-chartjs-2';

import Sticky from 'react-sticky-el';

import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

import { changeProfilePage } from '../../../actions'

class CompanyProfileView extends Component {


    constructor(props) {
        super(props) 

        this.state = {
            officePage:0,
            teamPage:0
        }
    }

    render() {

        let { officePage, teamPage } = this.state
        let { formValues, changePage, submit, culture } = this.props;

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        let currentCultureData = _.reduce(culture, function(data, n) {
            data.push({type:n.type.charAt(0).toUpperCase() + n.type.slice(1), color:getRandomColor(), number:n.data[0].current});
            return data
        }, []);

        let preferredCultureData = _.reduce(culture, function(data, n) {
            data.push({type:n.type.charAt(0).toUpperCase() + n.type.slice(1), color:getRandomColor(), number:n.data[1].preferred});
            return data
        }, []);

        let backgroundImage = _.get(formValues, "uploadcover", "");
        let avatar = _.get(formValues, "uploadlogo", "");
        let companyname = _.get(formValues, "companyname", "N/A");
        let industry = _.get(formValues, "industry", "N/A");
        let numberOfEmployees = _.get(formValues, "numberofemployees", "N/A");
        let phone = _.get(formValues, "phone", "N/A");
        let headquaters = _.get(formValues, "locations[0].address", "");

        let website = _.get(formValues, "website", "");
        let linkedin = _.get(formValues, "linkedin", "");
        let facebook = _.get(formValues, "facebook", "");
        let twitter = _.get(formValues, "twitter", "");
        let pinterest = _.get(formValues, "pinterest", "");
        let youtube = _.get(formValues, "youtube", "");
        let instagram = _.get(formValues, "instagram", "");
        let telegram = _.get(formValues, "telegram", "");
        let angel = _.get(formValues, "angel", "");
        let glassdoor = _.get(formValues, "glassdoor", "");

        let socialLinks = _.reduce([[{"name":"website"},{"link":website}],
            [{"name":"linkedin"},{"link":linkedin}],
            [{"name":"twitter"},{"link":twitter}],
            [{"name":"facebook"},{"link":facebook}],
            [{"name":"pinterest"},{"link":pinterest}],
            [{"name":"youtube"},{"link":youtube}],
            [{"name":"instagram"},{"link":instagram}],
            [{"name":"telegram"},{"link":telegram}],
            [{"name":"angel"},{"link":angel}],
            [{"name":"glassdoor"},{"link":glassdoor}]], function(data, element) {

            if(element[1].link !== ""){
                if (data[0].length < 4){
                    data[0].push(element);
                } else {
                    data[1].push(element);
                }
            }
            
            return data
        }, [[],[]]);

        let whatWeDo = _.get(formValues, "whatwedo", "N/A");
        let whyWeDoWhatWeDo = _.get(formValues, "whywedowhatwedo", "N/A");
        let weAreProudOf = _.get(formValues, "weareproudof", "N/A");

        let worklifeBalance = _.get(formValues, "perksandbenefits.worklifebalance", []);
        let compensation = _.get(formValues, "perksandbenefits.compensation", []);
        let environmentCommunity = _.get(formValues, "perksandbenefits.environmentcommunity", []);
        let wealthHealth = _.get(formValues, "perksandbenefits.wealthhealth", []);
        let developmentGrowth = _.get(formValues, "perksandbenefits.developmentgrowth", []);

        let ourOffice = _.get(formValues, "ouroffice", []);
        let members = _.get(formValues, "members", []);

        let representativeAvatar = _.get(formValues, "representative.avatar", "");
        let representativeFirstName = _.get(formValues, "representative.firstName", "N/A");
        let representativeLastName= _.get(formValues, "representative.lastName", "");
        let representativePosition = _.get(formValues, "representative.position", "N/A");

        let currentCompanyCulture = _.get(formValues, "currentcompanyculture", true);
        let preferredCompanyCulture = _.get(formValues, "preferredcompanyculture", true);

        const capitalize = (str) => {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }

        return (
            <div className="profile-view">
                {backgroundImage ? <img className="background-image" src={backgroundImage} alt="background"/>
                    : <div className="background-image"></div>}
                <Container>
                    <Row>
                        <Col className="profile-view-header" xs={12}>
                            <Media>
                                <Media left>
                                    {avatar ? <img className="avatar" src={avatar} alt="background"/>
                                        : <div className="avatar"></div>}
                                </Media>
                                <Media body>
                                    <div className="company-name">{companyname}</div>
                                    <div className="industry">{industry}</div>
                                    <div className="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div className="num-employees-container">
                                                <div className="num-employees">Number of employees</div>
                                                <div className="num-employees-value">{numberOfEmployees}</div>
                                            </div>
                                            <div className="phone-container">
                                                <div className="phone">Phone</div>
                                                <div className="phone-value">{phone}</div>
                                            </div>
                                            <div className="headquaters-container">
                                                <div className="headquaters">Headquaters</div>
                                                <div className="headquaters-value">{_.isEmpty(headquaters) ? "N/A" : headquaters}</div>
                                            </div>
                                        </div>
                                        <div>
                                            {!_.isEmpty(socialLinks[0]) && socialLinks[0].map((element, index) => 
                                                <Badge key={index} href={!_.includes(element[1].link,"https://") ? "https://"+element[1].link : element[1].link} target="_blank" className="social-btn">
                                                    {element[0].name === "website" && <FaGlobe size={18}/>}
                                                    {element[0].name === "linkedin" && <FaLinkedinIn size={18}/>}
                                                    {element[0].name === "facebook" && <FaFacebookF size={18}/>}
                                                    {element[0].name === "twitter" && <FaTwitter size={18}/>}
                                                    {element[0].name === "pinterest" && <FaPinterest size={18}/>}
                                                    {element[0].name === "youtube" && <FaYoutube size={18}/>}
                                                    {element[0].name === "instagram" && <FaInstagram size={18}/>}
                                                    {element[0].name === "telegram" && <FaTelegram size={18}/>}
                                                    {element[0].name === "angel" && <FaAngellist size={18}/>}
                                                    {element[0].name === "glassdoor" && <MdCropFree size={18}/>}
                                                </Badge>
                                            )}
                                            {!_.isEmpty(socialLinks[1]) && <UncontrolledDropdown tag="span">
                                                <DropdownToggle tag="span">
                                                    <Badge className="social-btn">
                                                        <MdMoreVert size={18}/>
                                                    </Badge>
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    {socialLinks[1].map((element, index) =>
                                                        <DropdownItem key={index} tag="a" target="_blank" href={element[1].link}>{capitalize(element[0].name)}</DropdownItem>)}
                                                </DropdownMenu>
                                            </UncontrolledDropdown>}
                                        </div>
                                    </div>
                                </Media>
                            </Media>
                        </Col>
                    </Row>
                    <Row className="profile-view-main">
                        <Col xs={12} md={8}>
                            <Card id="main-information">
                                <div className="main-information-title">What we do</div>
                                <div className="main-information-desc">{whatWeDo}</div>

                                <div className="main-information-title">Why we do what we do</div>
                                <div className="main-information-desc">{whyWeDoWhatWeDo}</div>

                                <div className="main-information-title">We are proud of</div>
                                <div className="main-information-desc without-margin">{weAreProudOf}</div>
                            </Card>
                            {(!_.isEmpty(worklifeBalance) || !_.isEmpty(compensation)  || !_.isEmpty(environmentCommunity)  || !_.isEmpty(wealthHealth)  || !_.isEmpty(developmentGrowth)) && <Card id="perks-and-benefits">
                                <div className="perks-and-benefits-title">Perks and Benefits</div>
                                
                                {!_.isEmpty(worklifeBalance) && <React.Fragment><div className="perks-and-benefits-section">Work-Life Balance</div>
                                    <div>
                                        {worklifeBalance.map((tag, key) => 
                                            <Badge className="perks-and-benefits-card" key={key}>
                                                <span className="align-middle">
                                                    {tag}
                                                </span>
                                            </Badge>
                                        )}
                                    </div></React.Fragment>}
                                
                                {!_.isEmpty(compensation) && <React.Fragment><div className="perks-and-benefits-section">Compensation</div>
                                    <div>
                                        {compensation.map((tag, key) => 
                                            <Badge className="perks-and-benefits-card" key={key}>
                                                <span className="align-middle">
                                                    {tag}
                                                </span>
                                            </Badge>
                                        )}
                                    </div></React.Fragment>}

                                {!_.isEmpty(environmentCommunity) && <React.Fragment><div className="perks-and-benefits-section">Environment & Community</div>
                                    <div>
                                        {environmentCommunity.map((tag, key) => 
                                            <Badge className="perks-and-benefits-card" key={key}>
                                                <span className="align-middle">
                                                    {tag}
                                                </span>
                                            </Badge>
                                        )}
                                    </div></React.Fragment>}

                                {!_.isEmpty(wealthHealth) && <React.Fragment><div className="perks-and-benefits-section">Wealth & Health</div>
                                    <div>
                                        {wealthHealth.map((tag, key) => 
                                            <Badge className="perks-and-benefits-card" key={key}>
                                                <span className="align-middle">
                                                    {tag}
                                                </span>
                                            </Badge>
                                        )}
                                    </div></React.Fragment>}

                                {!_.isEmpty(developmentGrowth) && <React.Fragment><div className="perks-and-benefits-section">Development & Growth</div>
                                    <div>
                                        {developmentGrowth.map((tag, key) => 
                                            <Badge className="perks-and-benefits-card" key={key}>
                                                <span className="align-middle">
                                                    {tag}
                                                </span>
                                            </Badge>
                                        )}
                                    </div></React.Fragment>}
                            </Card>}
                            {(!_.isEmpty(members) || !_.isEmpty(ourOffice)) && <Card id="office-and-team">
                                {!_.isEmpty(ourOffice) && <div>
                                    <ButtonToolbar className={(!ourOffice || !Array.isArray(ourOffice) || ourOffice.length === 0) ? "justify-content-start align-items-center" : "justify-content-between align-items-center"}>
                                        <div className="our-office-label">Our office</div>
                                        {(!ourOffice || !Array.isArray(ourOffice) || ourOffice.length === 0 ) ? null : <div className="arrow-btns">
                                            <Button type="button" disabled={(!ourOffice || !Array.isArray(ourOffice) || _.chunk(ourOffice, 3).length === 1 || officePage === 0) ? true : false} color="primary" size="lg" onClick={() => this.setState({officePage: officePage-1})}>
                                                <img className="back-icon align-middle" src={back} alt="back"/>
                                            </Button>
                                            <Button type="button" color="primary" size="lg" onClick={() => this.setState({officePage: officePage+1})} disabled={(!ourOffice || !Array.isArray(ourOffice) || _.chunk(ourOffice, 3).length === 1 || officePage+1 === _.chunk(ourOffice, 3).length ) ? true : false} >
                                                <img className="next-icon align-middle" src={next} alt="next"/>
                                            </Button>
                                        </div>}
                                    </ButtonToolbar>
                                    <Row>
                                        {ourOffice && Array.isArray(ourOffice) && _.chunk(ourOffice, 3)[officePage] &&
                                            _.chunk(ourOffice, 3)[officePage].map((image, index) => {
                                                return <Col xs={12} md={4} key={index}>
                                                    <div className="images-array d-table">
                                                        <div className="d-table-cell align-middle">
                                                            <img className="image-preview align-middle" src={image} alt="response"/>
                                                        </div>
                                                    </div>
                                                </Col>
                                            })
                                        }
                                    </Row>
                                </div>}
                                {!_.isEmpty(members) && <div>
                                    <ButtonToolbar className={(!members || !Array.isArray(members) || members.length === 0) ? "justify-content-start align-items-center" : "justify-content-between align-items-center"}>
                                        <div className="our-team-label">Our team</div>
                                        {(!members || !Array.isArray(members) || members.length === 0 ) ? null : <div className="arrow-btns">
                                            <Button type="button" disabled={(!members || !Array.isArray(members) || _.chunk(members, 3).length === 1 || teamPage === 0) ? true : false} color="primary" size="lg" onClick={() => this.setState({teamPage: teamPage-1})}>
                                                <img className="back-icon align-middle" src={back} alt="back"/>
                                            </Button>
                                            <Button type="button" color="primary" size="lg" onClick={() => this.setState({teamPage: teamPage+1})} disabled={(!members || !Array.isArray(members) || _.chunk(members, 3).length === 1 || teamPage+1 === _.chunk(members, 3).length ) ? true : false} >
                                                <img className="next-icon align-middle" src={next} alt="next"/>
                                            </Button>
                                        </div>}
                                    </ButtonToolbar>
                                    <Row>
                                        {members && Array.isArray(members) && _.chunk(members, 3)[teamPage] &&
                                            _.chunk(members, 3)[teamPage].map((member, index) => {
                                                return <Col xs={12} md={4} key={index}>
                                                    <div className="team-images-container">
                                                        <div className="images-team-array d-table">
                                                            <div className="d-table-cell align-middle">
                                                                <img className="image-preview align-middle" src={member.avatar} alt="response"/>
                                                            </div>
                                                        </div>
                                                        <div className="member-name">{member.name}</div>
                                                        <div className="member-position">{member.position}</div>
                                                    </div>
                                                </Col>
                                            })
                                        }
                                    </Row>
                                </div>}
                            </Card>}
                        </Col>
                        <Col xs={12} md={4}>
                            <Card id="representative">
                                <div className="representative-title">Company Representative</div>
                                {representativeAvatar ? <img className="representative-image" src={representativeAvatar} alt="response"/>
                                    : <div className="representative-image"></div>}
                                <div className="representative-name">{representativeFirstName+" "+representativeLastName}</div>
                                <div className="representative-position">{representativePosition}</div>
                            </Card>
                            {currentCompanyCulture && <Card id="culture">
                                <div className="culture-title">Company Culture (current)</div>
                                <div className="culture-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                <Doughnut height={180} options={{
                                    legend: {
                                        display: false
                                    },
                                    responsive: true
                                }} data={{
                                    labels: _.reduce(currentCultureData, function(data, labels) {data.push(labels.type.toUpperCase());return data}, []),
                                    datasets: [
                                        {
                                            backgroundColor:_.reduce(currentCultureData, function(data, colors) {data.push(colors.color);return data}, []),
                                            hoverBackgroundColor:_.reduce(currentCultureData, function(data, colors) {data.push(colors.color);return data}, []),
                                            data: _.reduce(currentCultureData, function(data, numbers) {data.push(numbers.number); return data}, [])
                                        }
                                    ]
                                }} />
                                <Table responsive>
                                    <tbody>
                                        {_.orderBy(currentCultureData, ['number'], ['desc']).map((item, index) => 
                                            <tr key={index}>
                                                <td className="align-middle"><div style={{width:"10px",height:"10px",backgroundColor:item.color}}></div></td>
                                                <td>{item.type}</td>
                                                <td>{item.number}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th>{_.reduce(culture, function(sum, n) {return sum + n.data[0].current;}, 0)}</th>
                                        </tr>
                                    </tfoot>
                                </Table>
                            </Card>}
                            {preferredCompanyCulture && <Card id="culture">
                                <div className="culture-title">Company Culture (preferred)</div>
                                <div className="culture-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                <Doughnut height={180} options={{
                                    legend: {
                                        display: false
                                    },
                                    responsive: true
                                }} data={{
                                    labels: _.reduce(preferredCultureData, function(data, labels) {data.push(labels.type.toUpperCase());return data}, []),
                                    datasets: [
                                        {
                                            backgroundColor:_.reduce(preferredCultureData, function(data, colors) {data.push(colors.color);return data}, []),
                                            hoverBackgroundColor:_.reduce(preferredCultureData, function(data, colors) {data.push(colors.color);return data}, []),
                                            data: _.reduce(preferredCultureData, function(data, numbers) {data.push(numbers.number); return data}, [])
                                        }
                                    ]
                                }} />
                                <Table responsive>
                                    <tbody>
                                        {_.orderBy(preferredCultureData, ['number'], ['desc']).map((item, index) => 
                                            <tr key={index}>
                                                <td className="align-middle"><div style={{width:"10px",height:"10px",backgroundColor:item.color}}></div></td>
                                                <td>{item.type}</td>
                                                <td>{item.number}</td>
                                            </tr>
                                        )}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th>{_.reduce(culture, function(sum, n) {return sum + n.data[1].preferred;}, 0)}</th>
                                        </tr>
                                    </tfoot>
                                </Table>
                            </Card>}
                        </Col>
                    </Row>
                </Container>
                <Sticky stickyStyle={{minHeight: "80px", zIndex: "5"}} mode="bottom" boundaryElement=".profile-view">
                    <div className="profile-submit">
                        <ButtonToolbar className="justify-content-center align-items-center">
                            <Button type="button" color="primary" outline size="lg" className="submit-btn" onClick={changePage}>Back</Button>{" "}
                            <Button type="submit" color="primary" size="lg" onClick={submit} className="submit-btn">Save</Button>
                        </ButtonToolbar> 
                    </div>
                </Sticky>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {

    const formValues = getFormValues(ownProps.form)(state);

    return {formValues};
}

export default connect(
    mapStateToProps,
    {
        changeProfilePage
    }
)(CompanyProfileView)
