import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, Alert, ButtonGroup, ButtonToolbar } from 'reactstrap';
import { FaLinkedinIn } from 'react-icons/fa';

import { signUpCandidate, linkedInAuth } from '../../../actions'

import TextField from '../fields/TextField'
import EmailField from '../fields/EmailField'
import PasswordField from '../fields/PasswordField'

import validate from '../validations/CandidateSignUpFormValidation'

class CandidateSignUpForm extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            loading: false
        }

        this.handleLinkedInAuth = this.handleLinkedInAuth.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {

        if (typeof window === 'undefined') {
            return;
        }

        this.loadSDK("77c4tf4tfcmxta")
    }

    loadSDK = clientId => {
    	;((d, s, id) => {
    		const element = d.getElementsByTagName(s)[0]
    		const ljs = element
    		let js = element
    		if (d.getElementById(id)) {
    			return
    		}
    		js = d.createElement(s)
    		js.id = id
    		js.src = `//platform.linkedin.com/in.js`
    		js.text = `api_key: ${clientId}`
    		ljs.parentNode.insertBefore(js, ljs)
    	})(document, 'script', 'linkedin-jssdk')
    }
    
    callBack = () => {

    	let { linkedInAuth } = this.props;

    	this.setState({ loading: true })

    	window.IN.API.Profile("me").fields(["firstName","lastName","pictureUrl","publicProfileUrl","emailAddress"]).result(result => {
            
    		this.setState({ loading: false })
            
    		linkedInAuth(result.values[0].firstName, result.values[0].lastName, result.values[0].emailAddress, result.values[0].pictureUrl,"candidateSignUpForm");
    	});
    }
    
    handleLinkedInAuth = e => {
    	e.preventDefault();

    	if (window.IN['User']) {
    		window.IN.User.authorize(this.callBack, '')
    	}
    }

    submit = values => {

    	let { signUpCandidate } = this.props;

    	signUpCandidate(values.firstname, values.lastname, values.email, values.password, "candidateSignUpForm");
    }

    render() {

  	    let { loading } = this.state
  	    let { error, handleSubmit, pristine, submitting, invalid } = this.props;
    
        return (
            <div>
                {error && <Alert color="danger">{error}</Alert>}
                <Alert color="warning">
                    Your personal information such as name, age, gender etc. will not be shared with companies. Your application will be considered without any bias, only based on your education, skills and experience.
                </Alert>
                <h3>Sign Up</h3>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <TextField form="candidateSignUpForm" name="firstname" label="First Name" shouldTrim={true} />
                    <TextField form="candidateSignUpForm" name="lastname" label="Last Name" shouldTrim={true} />
                    <EmailField form="candidateSignUpForm" name="email" label="Email" />
                    <PasswordField form="candidateSignUpForm" name="password" label="Password" />
                    <ButtonToolbar className="justify-content-between align-items-center">
                        <ButtonGroup>
                            <Button type="submit" disabled={pristine || submitting || invalid} color="primary" size="lg">Create account</Button>
                        </ButtonGroup>
                        <ButtonGroup>
                            <Link className="have-account" to={{pathname: '/login', state: { type: "candidate" }}} color="link">I have account</Link>
                        </ButtonGroup>
                    </ButtonToolbar>
                </Form>
                <div className="hr-or">
                    <span>or</span>
                </div>
                <Button color="primary" className="linkedin-button" block onClick={this.handleLinkedInAuth} disabled={submitting || loading}>
                    <FaLinkedinIn className="linkedin-icon align-middle"/>
                    <span className="align-middle">Sign Up with LinkedIn</span>
                </Button>
            </div>
        )
    }
};

CandidateSignUpForm = connect(
    null,
    {
        signUpCandidate,
        linkedInAuth
    }
)(CandidateSignUpForm);

export default reduxForm({
    initialValues: {
        
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'candidateSignUpForm',
    validate
})(CandidateSignUpForm)