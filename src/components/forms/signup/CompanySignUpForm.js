import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, Alert, ButtonGroup, ButtonToolbar} from 'reactstrap';

import { signUpCompany } from '../../../actions'

import TextField from '../fields/TextField'
import EmailField from '../fields/EmailField'
import PasswordField from '../fields/PasswordField'

import validate from '../validations/CompanySignUpFormValidation'

class CompanySignUpForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this);
    }

    submit = (values) => {

        let { signUpCompany } = this.props;

        signUpCompany(values.firstname, values.lastname, values.companyname, values.email, values.password, "companySignUpForm");
    }

    render() {

        let { error, handleSubmit, pristine, submitting, invalid } = this.props;

        return (
            <div>
                {error && <Alert color="danger">{error}</Alert>}
                <h3>Sign Up</h3>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <TextField form="companySignUpForm" name="firstname" label="First Name" shouldTrim={true} />
                    <TextField form="companySignUpForm" name="lastname" label="Last Name" shouldTrim={true} />
                    <TextField form="companySignUpForm" name="companyname" label="Company Name" shouldTrim={true} />
                    <EmailField form="companySignUpForm" name="email" label="Work Email" />
                    <PasswordField form="companySignUpForm" name="password" label="Password" />
                    <ButtonToolbar className="justify-content-between align-items-center">
                        <ButtonGroup>
                            <Button type="submit" disabled={pristine || submitting || invalid} color="primary" size="lg">Create account</Button>
                        </ButtonGroup>
                        <ButtonGroup>
                            <Link className="have-account" to={{pathname: '/login', state: { type: "company" }}} color="link">I have account</Link>
                        </ButtonGroup>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

CompanySignUpForm = connect(
    null,
    {
        signUpCompany
    }
)(CompanySignUpForm);

export default reduxForm({
    initialValues: {
        
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'companySignUpForm',
    validate
})(CompanySignUpForm)