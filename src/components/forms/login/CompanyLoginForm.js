import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, Alert, ButtonGroup, ButtonToolbar } from 'reactstrap';

import { loginCompany } from '../../../actions'

import EmailField from '../fields/EmailField'
import PasswordField from '../fields/PasswordField'

import validate from '../validations/CompanyLoginFormValidation'

class CompanyLoginForm extends Component {

    constructor(props) {
        super(props) 

        this.submit = this.submit.bind(this);
    }

    submit = (values) => {

        let { loginCompany } = this.props;

        loginCompany(values.email, values.password, "companyLoginForm");
    }

    render() {

        let { error, handleSubmit, pristine, submitting, invalid } = this.props;

        return (
            <div>
                {error && <Alert color="danger">{error}</Alert>}
                <h3>Login</h3>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <EmailField form="companyLoginForm" name="email" label="Email" />
                    <PasswordField form="companyLoginForm" name="password" label="Password" />
                    <ButtonToolbar className="justify-content-end align-items-center">
                        <Link to="reset-password" className="reset-password" color="link">Forgot password?</Link>
                    </ButtonToolbar>
                    <ButtonToolbar className="justify-content-between align-items-center">
                        <ButtonGroup>
                            <Button type="submit" disabled={pristine || submitting || invalid} color="primary" size="lg">Log In</Button>
                        </ButtonGroup>
                        <ButtonGroup>
                            <Link className="create-account" to={{pathname: '/signup', state: { type: "company" }}} color="link">Create account</Link>  
                        </ButtonGroup>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

CompanyLoginForm = connect(
    null,
    {
        loginCompany
    }
)(CompanyLoginForm);

export default reduxForm({
    initialValues: {
        
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'companyLoginForm',
    validate
})(CompanyLoginForm)