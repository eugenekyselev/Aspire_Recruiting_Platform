import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Button, Form, Alert, ButtonGroup, ButtonToolbar } from 'reactstrap';

import { FaLinkedinIn } from 'react-icons/fa';

import { loginCandidate, linkedInAuth } from '../../../actions'

import EmailField from '../fields/EmailField'
import PasswordField from '../fields/PasswordField'

import validate from '../validations/CandidateLoginFormValidation'

class CandidateLoginForm extends Component {

    constructor(props) {
        super(props) 

        this.state = {
            loading: false
        }

        this.handleLinkedInAuth = this.handleLinkedInAuth.bind(this);
        this.submit = this.submit.bind(this);
    }

    componentDidMount() {

        if (typeof window === 'undefined') {
            return;
        }

        this.loadSDK("77c4tf4tfcmxta")
    }

    loadSDK = clientId => {
        ;((d, s, id) => {
            const element = d.getElementsByTagName(s)[0]
            const ljs = element
            let js = element
            if (d.getElementById(id)) {
                return
            }
            js = d.createElement(s)
            js.id = id
            js.src = `//platform.linkedin.com/in.js`
            js.text = `api_key: ${clientId}`
            ljs.parentNode.insertBefore(js, ljs)
        })(document, 'script', 'linkedin-jssdk')
    }
      
    callBack = () => {

        let { linkedInAuth } = this.props;

        this.setState({ loading: true })

        window.IN.API.Profile("me").fields(["firstName","lastName","pictureUrl","publicProfileUrl","emailAddress"]).result(result => {
            
            this.setState({ loading: false })
            
            linkedInAuth(result.values[0].firstName, result.values[0].lastName, result.values[0].emailAddress, result.values[0].pictureUrl,"candidateLoginForm");
        });
    }
      
    handleLinkedInAuth = e => {
        e.preventDefault();

        if (window.IN['User']) {
            
            window.IN.User.authorize(this.callBack, '')
        }
    }

    submit = values => {

        let { loginCandidate } = this.props;

        loginCandidate(values.email, values.password, "candidateLoginForm");
    }

    render() {

        let { loading } = this.state
        let { error, handleSubmit, pristine, submitting, invalid } = this.props;

        return (
            <div>
                {error && <Alert color="danger">{error}</Alert>}
                <h3>Login</h3>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <EmailField form="candidateLoginForm" name="email" label="Email" />
                    <PasswordField form="candidateLoginForm" name="password" label="Password" />
                    <ButtonToolbar className="justify-content-end align-items-center">
                        <Link to="reset-password" className="reset-password" color="link">Forgot password?</Link>
                    </ButtonToolbar>
                    <ButtonToolbar className="justify-content-between align-items-center">
                        <ButtonGroup>
                            <Button type="submit" disabled={pristine || submitting || invalid} color="primary" size="lg">Log In</Button>
                        </ButtonGroup>
                        <ButtonGroup>
                            <Link className="create-account" to={{pathname: '/signup', state: { type: "candidate" }}} color="link">Create account</Link>
                        </ButtonGroup>
                    </ButtonToolbar>
                </Form>
                <div className="hr-or">
                    <span>or</span>
                </div>
                <Button color="primary" className="linkedin-button" block onClick={this.handleLinkedInAuth} disabled={submitting || loading}>
                    <FaLinkedinIn className="linkedin-icon align-middle"/>
                    <span className="align-middle">Login with LinkedIn</span>
                </Button>
            </div>
        );
    }
};

CandidateLoginForm = connect(
    null,
    {
        loginCandidate,
        linkedInAuth
    }
)(CandidateLoginForm);

export default reduxForm({
    initialValues: {
        
    },
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    form: 'candidateLoginForm',
    validate
})(CandidateLoginForm)