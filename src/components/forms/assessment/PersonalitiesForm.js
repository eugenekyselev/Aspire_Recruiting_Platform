import React, { Component } from 'react';
import { formValueSelector, FormSection, Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Table, Row, Col, Button, Label, Form, FormGroup, Alert, Input, ButtonToolbar } from 'reactstrap';
import _ from 'lodash';

import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

class PersonalitiesForm extends Component {

    constructor(props) {
        super(props) 

        this.skipTest = this.skipTest.bind(this);
    }

    skipTest = () => {

        let { destroy, clearPage } = this.props

        destroy();

        clearPage();
    }

    renderField = ({ input, label, type, meta: { touched, error }, options, amountOfSelected  }) => (
        <FormGroup className="check " disabled={false}>
            <Label className="check w-100">
                <Input disabled={!input.value && amountOfSelected === 7 ? true : false} type="checkbox" {...input} value={options.value} checked={input.value} />
                <div className="checkmark d-flex justify-content-center align-items-center">{options.values.map((value, i) => <React.Fragment key={i}>{value}<br/></React.Fragment>)}</div>
            </Label>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    render() {

        let { personalities, amountOfSelected, current, amount, testArray, error, handleSubmit, previousPage, pristine, submitting, invalid } = this.props;

        return (
            <div className="personalities-assessment">
                <Form onSubmit={handleSubmit}>
                    <div className="personalities-assessment-title">Personalities</div>
                    <Row className="justify-content-center align-items-center">
                        <Col lg="9" md="8" xs="12">
                            <div className="personalities-assessment-desc">What are the main personality traits among your current team?<br/>Select 7 groups from the list below</div>
                            <div className="personalities-assessment-details">Approximate passing time 1 min 48 sec</div>
                        </Col>
                        <Col lg="3" md="4" xs="12"/>
                    </Row>
                    {error && <Alert color="danger">{error}</Alert>}
                    <Table responsive>
                        <tbody>
                            {testArray.map((testRow,y) => (
                                <tr key={y}>
                                    {testRow.map((values, i) => 
                                        <td key={i}>
                                            <div>
                                                <FormSection name="result">
                                                    <Field name={(testRow.length*y+i+((current-1)*28)).toString()}
                                                        options={{ values: values, value: false }} 
                                                        component={this.renderField} amountOfSelected={amountOfSelected}
                                                    />
                                                </FormSection>
                                            </div>
                                        </td>
                                    )}
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                    <ButtonToolbar className={_.isEmpty(personalities) ? "justify-content-between align-items-center" : "justify-content-end align-items-center"}>
                        {_.isEmpty(personalities) && <Button type="button" className="skip-test" color="link" onClick={this.skipTest}>Skip test</Button>}
                        <div>
                            <Button type="submit" disabled={pristine || submitting || invalid || amountOfSelected !== 7 } color="link" className="next-test">Next test</Button>
                        </div>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

const selector = formValueSelector('personalitiesForm')

const mapStateToProps = (state, ownProps) => {

    let amountOfSelected = _.size(_.reduce(_.omitBy(selector(state, "result"), _.isNil), function(result = [], value, key) {
        if(value === true) {
            result.push(value)
        }
        return result;
    }, []));

    let odd = _.reduce(ownProps.pageWithTest, function(result, value, key) {

        if(key % 2 === 0) {
            result.push(value)
        }
        return result;
    }, [])

    let even = _.reduce(ownProps.pageWithTest, function(result, value, key) {

        if(key % 2 !== 0) {
            result.push(value)
        }
        return result;
    }, [])

    let testArray = [odd, even]

    return {
        amountOfSelected: amountOfSelected,
        testArray: testArray,
        personalities: state.assessment.personalities
    }
}

PersonalitiesForm = connect(
    mapStateToProps,
    {
        
    }
)(PersonalitiesForm);


export default reduxForm({
    form: 'personalitiesForm',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(PersonalitiesForm)