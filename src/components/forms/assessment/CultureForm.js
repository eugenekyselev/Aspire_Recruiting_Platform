import React, { Component } from 'react';
import { formValueSelector, FormSection, Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Table, Row, Col, Button, Form, FormGroup, Alert, Input, ButtonToolbar } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';

import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

class CultureForm extends Component {

    constructor(props) {
        super(props) 

        this.isNumber = this.isNumber.bind(this);
        this.isInt = this.isInt.bind(this);

        this.futureValuesIsBetween = this.futureValuesIsBetween.bind(this);
        this.nowValuesIsBetween = this.nowValuesIsBetween.bind(this);

        this.skipTest = this.skipTest.bind(this);
    }

    skipTest = () => {

        let { destroy, clearPage } = this.props

        destroy();
        clearPage();
    }

    renderField = ({ input, label, type, meta: { touched, error }, futuresum, nowsum  }) => (
        <FormGroup>
            <Input onPaste={(evt) => evt.preventDefault()} onKeyPress={(evt) => {if (evt.key && evt.key !== "Backspace" && evt.key !== "0" && evt.key !== "1" && evt.key !== "2" && evt.key !== "3" && evt.key !== "4" && evt.key !== "5" && evt.key !== "6" && evt.key !== "7" && evt.key !== "8" && evt.key !== "9") {evt.preventDefault();}}}  {...input} placeholder={label} className={classnames({invalid: (touched && error) ? true : false })} type={type} autoComplete="off" disabled={!input.value && (nowsum === 0 ? true : false || futuresum === 0 ? true : false)}/>
            {touched && error && <div className="text-danger"><small>{error}</small></div>}
        </FormGroup>
    )

    isNumber = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined

    isInt = value => typeof value === "undefined" ? undefined : Number.isInteger(Number(value)) ? undefined : 'Must be an integer number'

    nowValuesIsBetween = (min, max) =>  (value, previousValue, allValues) => {

        let { current } = this.props

        if (!value) {
            return "";
        }

        let nowSum = _.reduce(allValues["page"+current].now ,function(sum, value) {
            return sum + Number(value);
        },0);

        if (Number(value) < min) {
            return previousValue
        } 

        if(Number(value) > max) {
            return previousValue
        }

        if(nowSum > max) {
            return previousValue
        }

        return value
    }

    futureValuesIsBetween = (min, max) => (value, previousValue, allValues) => {

        let { current } = this.props
        
        if (!value) {
            return "";
        }

        let futureSum = _.reduce(allValues["page"+current].future ,function(sum, value) {
            return sum + Number(value);
        },0);

        if (Number(value) < min) {
            return previousValue
        } 

        if(Number(value) > max) {
            return previousValue
        }

        if(futureSum > max) {
            return previousValue
        }

        return value
    }

    render() {

        let { culture, nowAmount, futureAmount, current, amount, pageWithTest, error, handleSubmit, previousPage, pristine, submitting, invalid } = this.props;

        return (
            <div className="culture-assessment">
                <Form onSubmit={handleSubmit}> 
                    <div className="culture-assessment-title">Culture</div>
                    <Row className="justify-content-center align-items-center">
                        <Col lg="9" md="8" xs="12">
                            <div className="culture-assessment-desc">These six questions ask you to identify the way you experience your organization right now, and, separately, the way you think it should be in the future</div>
                            <div className="culture-assessment-details">You may divide the 100 points in any way among the four alternatives in each question.  Some alternatives may get zero points, for example. Remember that the total must equal 100.</div>
                        </Col>
                        <Col lg="3" md="4" xs="12">
                            <div className="float-right pages">{current} / {amount}</div>
                        </Col>
                    </Row>
                    {error && <Alert color="danger">{error}</Alert>}
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{pageWithTest.title}</th>
                                <th>#</th>
                                <th>Now</th>
                                <th>#</th>
                                <th>Future</th>
                            </tr>
                        </thead>
                        <tbody>
                            {pageWithTest.test.map((value,i) => (
                                <tr key={i}>
                                    <td width="40px" align="right">{value.key}.</td>
                                    <td>{value.question}</td>
                                    <td className="td-valign">{value.key}.</td>
                                    <td className="td-valign">
                                        <FormSection name={`page${current}.now`}>
                                            <Field name={value.key} type="number" 
                                                component={this.renderField} label="0" normalize={this.nowValuesIsBetween(1,100)}
                                                validate={[ this.isNumber, this.isInt ]} nowsum={nowAmount}
                                            />
                                        </FormSection>
                                    </td>
                                    <td className="td-valign">{value.key}.</td>
                                    <td className="td-valign">
                                        <FormSection name={`page${current}.future`}>
                                            <Field name={value.key} type="number" 
                                                component={this.renderField} label="0" normalize={this.futureValuesIsBetween(1,100)}
                                                validate={[ this.isNumber, this.isInt ]} futuresum={futureAmount}
                                            />
                                        </FormSection>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th>Sum</th>
                                <th></th>
                                <th className="text-center">
                                    {nowAmount}
                                </th>
                                <th></th>
                                <th className="text-center">
                                    {futureAmount}
                                </th>
                            </tr>
                        </tfoot>
                    </Table>
                    <ButtonToolbar className={_.isEmpty(culture) ? "justify-content-between align-items-center" : "justify-content-end align-items-center"}>
                        {_.isEmpty(culture) && <Button type="button" className="skip-test" color="link" onClick={this.skipTest}>Skip test</Button>}
                        <div>
                            <Button type="button" disabled={current === 1 || submitting} color="primary" size="lg" onClick={previousPage}>
                                <img className="back-icon align-middle" src={back} alt="back"/>
                                <span className="align-middle">Back</span>
                            </Button>
                            {current === amount ? 
                                <Button type="submit" disabled={pristine || submitting || invalid || nowAmount !== 0 || futureAmount !== 0} color="link" className="next-test">Next test</Button>
                                :
                                <Button type="submit" disabled={pristine || submitting || invalid || nowAmount !== 0 || futureAmount !== 0} color="primary" size="lg">
                                    <span className="align-middle">Next</span>
                                    <img className="next-icon align-middle" src={next} alt="back"/>
                                </Button>
                            }
                        </div>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

const selector = formValueSelector('cultureForm')

const mapStateToProps = (state, ownProps) => {

    let futureSum = _.reduce(selector(state, `page${ownProps.current}.future`) ,function(sum, value) {
        return sum + Number(value);
    },0);

    let nowSum = _.reduce(selector(state, `page${ownProps.current}.now`) ,function(sum, value) {
        return sum + Number(value);
    },0);

    let futureAmount = 100 - futureSum;
    let nowAmount = 100 - nowSum;

    return {
        futureAmount: futureAmount,
        nowAmount: nowAmount,
        culture: state.assessment.culture
    }
}

CultureForm = connect(
    mapStateToProps,
    {
        
    }
)(CultureForm);


export default reduxForm({
    form: 'cultureForm',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(CultureForm)