import React, { Component } from 'react';
import { formValueSelector, getFormValues, FormSection, Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Table, Row, Label, Col, Button, Form, FormGroup, Alert, Input, ButtonToolbar } from 'reactstrap';
import _ from 'lodash';

import back from '../../../assets/img/assessment/back.svg'
import next from '../../../assets/img/assessment/next.svg'

class ValuesForm extends Component {

    constructor(props) {
        super(props) 

        this.skipTest = this.skipTest.bind(this);
    }

    skipTest = () => {

        let { destroy, clearPage } = this.props

        destroy();

        clearPage();
    }

    renderField = ({ input, meta: { touched, error }, options  }) => (
        options.map(o => (
            <td width="82px" key={o.value}  className="td-valign">
                <FormGroup disabled={false}>
                    <Label className="radio"><span>{o.title}</span>
                        <Input type="radio" {...input} value={o.value} checked={o.value === input.value} />
                        <span className="checkround"></span>
                    </Label>
                    {touched && error && <div className="text-danger"><small>{error}</small></div>}
                </FormGroup>
            </td>))
    )

    render() {

        let { formValues, values, current, allSelected, amount, pageWithTest, error, handleSubmit, previousPage, pristine, submitting, invalid } = this.props;

        return (
            <div className="values-assessment">
                <Form onSubmit={handleSubmit}>
                    <div className="values-assessment-title">Values</div>
                    <Row className="justify-content-center align-items-center">
                        <Col lg="9" md="8" xs="12">
                            <div className="values-assessment-desc">Which statements best represent your organisation</div>
                            <div className="values-assessment-details">Approximate passing time 2 min 32 sec</div>
                        </Col>
                        <Col lg="3" md="4" xs="12">
                            <div className="float-right pages">{current} / {amount}</div>
                        </Col>
                    </Row>
                    {error && <Alert color="danger">{error}</Alert>}
                    <Table striped responsive>
                        <tbody>
                            {pageWithTest.map((value,i) => (
                                <tr key={i}>
                                    <td width="38px" align="right">{i+1+((current-1)*12)}.</td>
                                    <td width="512px">{value}</td>                            
                                    <FormSection name="result">
                                        <Field name={(i+((current-1)*12)).toString()}
                                            options={[{ title: 'Yes', value: 'yes' }, { title: 'No', value: 'no' }]} 
                                            component={this.renderField} 
                                        />
                                    </FormSection>
                                    <td width="108px"></td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                    <ButtonToolbar className={_.isEmpty(values) ? "justify-content-between align-items-center" : "justify-content-end align-items-center"}>
                        {_.isEmpty(values) && <Button type="button" className="skip-test" color="link" onClick={this.skipTest}>Skip test</Button>}
                        <div>
                            <Button type="button" disabled={current === 1 || submitting} color="primary" size="lg" onClick={previousPage}>
                                <img className="back-icon align-middle" src={back} alt="back"/>
                                <span className="align-middle">Back</span>
                            </Button>
                            {current === amount ? 
                                <Button type="submit" disabled={pristine || submitting || invalid || !allSelected } color="link" className="next-test">Done</Button>
                                :
                                <Button type="submit" color="primary" size="lg" disabled={!formValues || (formValues && _.size(_.omitBy(formValues.result, _.isNil)) < current*12)}>
                                    <span className="align-middle">Next</span>
                                    <img className="next-icon align-middle" src={next} alt="back"/>
                                </Button>
                            }
                        </div>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
};

const selector = formValueSelector('valuesForm')

const mapStateToProps = (state, ownProps) => {

    
    let allSelected = _.size(_.omitBy(selector(state, "result"), _.isNil)) === ownProps.allAmount ? true : false;

    return {
        allSelected: allSelected,
        values: state.assessment.values,
        formValues: getFormValues('valuesForm')(state)
    }
}

ValuesForm = connect(
    mapStateToProps,
    {
        
    }
)(ValuesForm);


export default reduxForm({
    form: 'valuesForm',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true
})(ValuesForm)