import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { HiddenOnlyAuth, VisibleOnlyAuth } from '../../wrappers'
import _ from 'lodash';
import classnames from 'classnames';
import { Container, Button, Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import {
    MdNotificationsNone,
    MdMailOutline
} from 'react-icons/md';

import avatar from '../../assets/img/no-avatar.png'
import logo from '../../assets/img/aspire-logo.svg'

import { logout } from '../../actions'
import { setPage, setActivePage } from '../../actions'

class Header extends Component {
 
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
        
    toggle() {

        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {

        let { pathname, role, profile, logout } = this.props;

        const OnlyAuthLinks = VisibleOnlyAuth(() =>
            <React.Fragment>
                <Nav className="auth align-items-center mr-auto" navbar pills>
                    <NavItem>
                        <NavLink tag={Link} to="/jobs" className={classnames({ active: (pathname === '/jobs' || pathname === '/jobs/create') })}>Jobs</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={Link} to="#" className={classnames({ active: pathname === '/candidates' })}>Candidates</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={Link} to="/shortlist" className={classnames({ active: pathname === '/shortlist' })}>Shortlist</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={Link} to="/profile" className={classnames({ active: pathname === '/profile' })}>{role === "candidate" ? "Candidate Profile" : role === "company" ? "Company Profile" : role === "admin" ? "Admin Profile" : "Profile"}</NavLink>
                    </NavItem>
                    {role === "candidate" ? 
                        <NavItem>
                            <NavLink tag={Link} to="#" className={classnames({ active: pathname === '/wallet' })}>Wallet</NavLink>
                        </NavItem> 
                        :
                        null
                    }
                </Nav>
                <Nav className="auth align-items-center ml-auto" navbar>
                    <NavItem>
                        <MdMailOutline className="inbox can-click" size={25} />
                    </NavItem>
                    <NavItem>
                        <MdNotificationsNone className="notifications can-click" size={25} />
                    </NavItem>
                    <NavItem>
                        {role === 'candidate' ? 
                            <ul className="list-unstyled">
                                <li><span className="username">{profile.firstname + " " + profile.lastname}</span></li>
                            </ul>  
                            :
                            role === 'company' ? 
                                <ul className="list-unstyled">
                                    <li><span className="company">{profile.company}</span></li>
                                    <li><span className="username">{profile.firstname + " " + profile.lastname}</span></li>
                                </ul>  
                                :
                                role === 'admin' ? 
                                    <ul className="list-unstyled">
                                        <li><span className="username">{profile.firstname + " " + profile.lastname}</span></li>
                                    </ul>  
                                    :
                                    "No Name"
                        }
                    </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>              
                            {_.has(profile, "avatar") ? 
                                <img src={profile.avatar} className="avatar" alt="avatar"/>
                                :
                                <img src={avatar} className="avatar" alt="avatar"/>
                            } 
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                Settings
                            </DropdownItem>
                            <DropdownItem onClick={() => {logout()}}>
                                Logout
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </React.Fragment>
        )

        const OnlyGuestLinks = HiddenOnlyAuth(() =>
            <Nav className="no-auth ml-auto align-items-center" navbar>
                <NavItem>
                    <NavLink tag={Link} to="/">Home</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} to="#">Jobs</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} to="#">Candidates</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} to="#">Companies</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} to="#">Hubs</NavLink>
                </NavItem>
                <div className="vdivide"/>
                <NavItem>
                    <NavLink tag={Link} to="/login">LOGIN</NavLink>
                </NavItem>
                <NavItem>
                    <Button color="primary" className="btn-sign-up" tag={Link} to={{pathname: '/signup', state: { type: "company" }}}>Sign Up</Button>
                </NavItem>
            </Nav>
        )

        return (
            <Navbar className="app-header" light expand="lg">
                <Container>
                    <NavbarBrand tag={Link} to="/">
                        <img src={logo} className="aspire-logo" alt="logo"/>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <OnlyGuestLinks />
                        <OnlyAuthLinks />
                    </Collapse>
                </Container>
            </Navbar>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pathname: state.router.location.pathname,
        role: state.user.role,
        profile: state.user.data
    }
}

export default connect(mapStateToProps, {logout, setActivePage, setPage})(Header);
