import React, { Component } from 'react';
import { connect } from 'react-redux'

import { Alert, 
    Container, 
    Row, 
    Col 
} from 'reactstrap';

import { resetError } from '../../actions'

class Error extends Component {
        
    componentWillUnmount(){

        let { resetError } = this.props;

        resetError();
    }

    render() {

        let { error, resetError } = this.props;

        return (
            <div>
                {(error !== null) ? 
                    <Container>
                        <Row className="my-4 justify-content-center align-items-center">
                            <Col md={6} lg={6}>
                                <Alert className="mb-0" color="danger" toggle={resetError}>
                                    {error}
                                </Alert>
                            </Col>
                        </Row>
                    </Container>
                    :
                    null
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        error: state.error
    }
}

export default connect(mapStateToProps, {resetError})(Error);
