import React, { Component,  } from 'react';
import { Pagination, PaginationItem, PaginationLink, Container, Row} from 'reactstrap';
import leftEnabledArrow from "../../assets/img/jobs_list/left-enabled-arrow.svg"
import rightEnabledArrow from "../../assets/img/jobs_list/right-enabled-arrow.svg"
import leftDisabledArrow from "../../assets/img/jobs_list/left-disabled-arrow.svg"
import rightDisabledArrow from "../../assets/img/jobs_list/right-disabled-arrow.svg"
import {connect, bindActionCreators} from "react-redux";
import { setPage, setActivePage } from '../../actions'
import PaginationItemComponent from './PaginationItemComponent'


 class PaginationComponent extends React.Component {

    disableLeftRight = position => {this.setState({disableLeft: position == 0, disableRight: position >= this.props.maxPagesCount - 4})}

    constructor(props) {
        super(props);

        this.disableLeftRight = this.disableLeftRight.bind(this);

        let startPosition = (this.props.startPosition.filter(elem => elem.pagename == this.props.name))
        let position = (startPosition != NaN && startPosition != undefined && startPosition.length > 0) ? startPosition[0].value : 0

        let activePosition = (this.props.activePosition.filter(elem => elem.pagename == this.props.name))
        let active = (activePosition != NaN && activePosition != undefined && activePosition.length > 0) ? activePosition[0].value : 1
        this.state = {startPosition: position, activePosition: active, disableLeft: true, disableRight: true}

        this.shouldShow = this.shouldShow.bind(this);
        this.itemsList = this.itemsList.bind(this);
        this.updatePosition = this.updatePosition.bind(this);
        this.moveTo = this.moveTo.bind(this)
        this.updateUI = this.updateUI.bind(this);
        this.changeActivePage = this.changeActivePage.bind(this);

        this.saveLink = (link, position) => {
           this["link"+position] = link
        };
    }

    componentDidMount() {

        let startPosition = (this.props.startPosition.filter(elem => elem.pagename == this.props.name))
        let position = (startPosition != NaN && startPosition != undefined && startPosition.length > 0) ? startPosition[0].value : 0
        this.disableLeftRight(position)
    }

    shouldShow = position => {return ( position > this.state.startPosition && position <= this.props.maxPagesCount)}

    itemAtPosition = position => {
          return (this.shouldShow(position + this.state.startPosition) &&  <PaginationItemComponent key={position + this.state.startPosition} rest={{active: (this.state.activePosition === (position + this.state.startPosition)), linkClicked: () => this.changeActivePage(position+this.state.startPosition), content: this.state.startPosition + position, position: this.state.startPosition+position}} forwardedRef={link => this.saveLink(link, position+this.state.startPosition)}/>)
    }

     itemsList = () => {
        var items = [];
        for(var i = 1; i <= 4; i++) {
            let temp = this.itemAtPosition(i);
            if (temp !== false) {
                items.push(temp);
            }
        }

        return items
    }

    updateUI = position => {
        this.setState({startPosition: position});
        let {setPage} = this.props
        setPage({pagename: this.props.name, value: position});
        this.disableLeftRight(position);
    }

     updatePosition(mark) {
        let startPosition = this.state.startPosition;

        if (mark === "+") {
            let position = (startPosition + 4 <= this.props.maxPagesCount) ? startPosition+4 : startPosition
            this.updateUI(position);
            this.changeActivePage(position+1)
            return
        }

        let position = (startPosition - 4 >= 0) ? startPosition - 4 : 0
         this.updateUI(position);
         this.changeActivePage(position != 0? position : 4)
        }

        moveTo(mark) {
            let startPosition = this.state.startPosition;
            if (mark === "end") {
                let position =  startPosition < this.props.maxPagesCount - 4 ? this.props.maxPagesCount - 4 : startPosition
                if (position != startPosition) {
                    this.updateUI(position);
                    this.changeActivePage(this.props.maxPagesCount)
                }
                return
            }

            let position =  0 // startPosition >= 4 ? 0 : startPosition
            if (position != startPosition) {
                this.updateUI(position);
                this.changeActivePage(position+1)
            }
        }

    changeActivePage(index) {
        if (index != this.state.activePosition) {
            let {setActivePage} = this.props;
            setActivePage({pagename: this.props.name, value: index});
            this.setState({activePosition: index})
        }
    }

    render() {
         let { activePosition } = this.state

        return (
    <section className="text-center justify-content-center">
        <Container className="align-in-center">
            <Pagination aria-label="Page navigation example" className="ml-0 mr-0 align-in-center">
                <PaginationItem disabled={ this.state.disableLeft } >
                    <PaginationLink onClick={() => this.moveTo("start")} className="border-0 arrow-top-margin" previous href="#"><img src={ this.state.disableLeft ?  leftDisabledArrow : leftEnabledArrow} alt="..."/></PaginationLink>
                </PaginationItem>
                <PaginationItem disabled={ this.state.disableLeft } hidden={this.state.disableLeft}>
                    <PaginationLink onClick={() => this.updatePosition("-")} className="border-0" href="#">
                        <span className={this.state.disableLeft ? "disabled-dots dots-settings" : "enabled-dots dots-settings"}>...</span>
                    </PaginationLink>
                </PaginationItem>
                    {this.itemsList()}
                <PaginationItem disabled={ this.state.disableRight} hidden={this.state.disableRight}>
                    <PaginationLink onClick={() => this.updatePosition("+")} className="border-0" href="#">
                        <span className={this.state.disableRight ? "disabled-dots" : "enabled-dots dots-settings"}>...</span>
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem disabled={ this.state.disableRight }>
                    <PaginationLink onClick={() => this.moveTo("end")} className="border-0 arrow-top-margin" next href="#"><img src={ this.state.disableRight ?  rightDisabledArrow : rightEnabledArrow} alt="..."/></PaginationLink>
                </PaginationItem>
            </Pagination>
        </Container>
    </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        startPosition: state.paginationtReducer.startPosition,
        activePosition: state.paginationtReducer.activePosition
    }
}

export default connect(mapStateToProps, {setPage, setActivePage})(PaginationComponent);