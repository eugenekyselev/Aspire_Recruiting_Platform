import React from 'react';

import Footer from './Footer';
import Header from './Header';
import Error from './Error';
import PageWithPagination from "../../pages/base/PageWithPagination";

const PaginatedAppLayout = ({ children }) => (

    <main>
        <Header/>
        <Error />
        <section className="app-container">
            <PageWithPagination>
                {children}
            </PageWithPagination>
        </section>
        <Footer />
    </main>
);

export default PaginatedAppLayout;