import React, { Component } from 'react';
import { DotLoader } from 'react-spinners';
import CenteredBlock from './CenteredBlock';

class Loading extends Component {
 
    render() {
        return (
            <CenteredBlock component={
                () => <DotLoader color={'#8a8a8f'} loading={true} />
            } />
        );
    }
}


export default Loading
