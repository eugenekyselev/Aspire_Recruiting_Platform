import React from "react";
import {PaginationItem, PaginationLink } from 'reactstrap';
import {connect} from "react-redux";

function withRefComponent(Component) {
    class CustomComponent extends React.Component {
        render() {
            let forwardedRef = this.props.props.forwardedRef;
            let rest = this.props.props.rest;

            return <Component ref={forwardedRef} rest={rest} />;
        }
    }

    return React.forwardRef((props, ref) => {
        return <CustomComponent props={props} forwardedRef={ref} />;
    });
}

class PaginationItemComponent extends React.Component {


    constructor(props) {
        super(props);
        this.forceClick = this.forceClick.bind(this);
    }

    forceClick = () => {
        let position = this.props.rest.position
        let element = document.querySelector("#plink")
        element.classList.toggle('active')
    }

    render() {
        const rest = this.props.rest;
        const forwardedRef = this.props.forwardedRef;
        return (
            <PaginationItem active={rest.active}>
                <PaginationLink  onClick={rest.linkClicked} className="border-0 pagination-number disable-outline" href="#">{rest.content}</PaginationLink>
            </PaginationItem>
        );
    }
}

export default withRefComponent(PaginationItemComponent);