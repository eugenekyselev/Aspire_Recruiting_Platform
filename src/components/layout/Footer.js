import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { 
    Container,
    Nav,
    NavItem,
    NavLink, 
    Row, 
    Col } from 'reactstrap';

import logo from '../../assets/img/aspire-logo.svg'

class Footer extends Component {

    render() {

        return (
            <footer>
                <Container>
                    <Row>
                        <Col lg="4" md="12" xs="12" className="text-center">
                            <img src={logo} className="aspire-logo-footer" alt="logo"/>
                        </Col>
                        <Col lg="2" md="6" xs="12">
                            <h5>ASPIRE</h5>
                            <Nav vertical>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Features</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Pricing</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Support</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">About Us</NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                        <Col lg="2" md="6" xs="12">
                            <h5>HUBS</h5>
                            <Nav vertical>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Join a hub</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Create a hub</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">View hubs</NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                        <Col lg="2" md="6" xs="12">
                            <h5>FOR RECRUITERS</h5>
                            <Nav vertical>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Post a job</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Company profile</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Find candidates</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Sign in</NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                        <Col lg="2" md="6" xs="12">
                            <h5>FOR CANDIDATES</h5>
                            <Nav vertical>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Browse jobs</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">Personal profile</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={Link} to="#">View hubs</NavLink>
                                </NavItem>
                            </Nav>
                        </Col>
                    </Row>
                </Container>
            </footer>
        );
    }
}

export default Footer;