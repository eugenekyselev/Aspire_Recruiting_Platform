import React from 'react';
import { 
    Container,
    Row 
} from 'reactstrap';

const CenteredBlock = ({ component: Component }) => (
    
    <section className="full-screen">
        <Container className="h-100">
            <Row className="h-75 justify-content-center align-items-center">
                <div className="text-center">
                    <Component />
                </div>
            </Row>
        </Container>
    </section>
        
);

export default CenteredBlock;