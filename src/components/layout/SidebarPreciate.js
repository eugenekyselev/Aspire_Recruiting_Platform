import React from "react";
import Sidebar from "react-sidebar";

const mql = window.matchMedia(`(max-width: 768px)`);

class SidebarPreciate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sidebarDocked: mql.matches,
            sidebarOpen: this.props.shouldOpenSidebar && mql.matches
        };

        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
    }

    render() {

        let {content, children} = this.props
        return (
            <Sidebar
                sidebar={content}
                open={this.props.shouldOpenSidebar && mql.matches}
                docked={this.props.shouldOpenSidebar && mql.matches}
                onSetOpen={this.onSetSidebarOpen}
                touchHandleWidth={0}
                dragToggleDistance={0}
                styles={{
                    sidebar: { background: "white"},
                    root: { position: "relative", bottom: 100},
                    overlay: { position: "relative"},
                    content: { position: "relative"}}}>
                {children}
                </Sidebar>
        );
    }
}

export default SidebarPreciate;