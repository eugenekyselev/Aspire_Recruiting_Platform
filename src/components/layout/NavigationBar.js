import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, Button, Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import classnames from 'classnames';

class NavigationBar extends Component {

    constructor(props) {
        super(props);

        this.toggleType = this.toggleType.bind(this);
        this.toggleOpen = this.toggleOpen.bind(this);
        this.state = {
            isOpen: false,
            activeTab: props.selectedState
        };

    }

    toggleType(state) {
        this.setState({activeTab: state})
        this.props.afterClick(state);
    }

    toggleOpen() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {
        let { activeTab } = this.state

        let pathname = this.props.pathname;

        const items = this.props.items;

        const listItems = items.map((item, index) => <NavItem className={this.props.fontName} key={item.state}>
            <NavLink className={classnames({ active: (item.state === this.props.selectedState)})} onClick={() => this.toggleType(item.state)}>{item.name}</NavLink>
        </NavItem>);

        return (<section className="d-none d-sm-none d-md-none d-lg-block d-xl-block">
                    <Navbar className="preciate-navbar p-0" light expand="lg">
                        <Container className="p-0">
                            <NavbarToggler onClick={this.toggleOpen} />
                            <Collapse isOpen={this.state.isOpen} navbar>
                                <Nav className="align-items-center mr-auto" navbar pills>
                                    {listItems}
                                </Nav>
                            </Collapse>
                        </Container>
                    </Navbar>
            </section>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        pathname: state.router.location.pathname,
    }
}

export default connect(mapStateToProps)(NavigationBar);
