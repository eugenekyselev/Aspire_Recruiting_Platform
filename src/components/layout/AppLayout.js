import React from 'react';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Footer from './Footer'; 
import Header from './Header';
import Error from './Error';

const AppLayout = ({ children }) => (
    <main>
        <Header />
        <Error />
        <section className="app-container">
            {children}
        </section>
        <ToastContainer autoClose={2000} />
        <Footer />
    </main>
);

export default AppLayout;