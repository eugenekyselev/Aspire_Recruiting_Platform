import React, { Component } from 'react';
import { Container,
    Card, CardBody, CardTitle, Row, Col, Button, Badge, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import doner from "../../../assets/img/jobs_list/group-2.svg"
import {goBack, push} from "connected-react-router";
import {clearAssessment} from "../../../actions";
import {destroy} from "redux-form";
import {connect} from "react-redux";

class JobSmallCard extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    donerClicked = (event) => {
        this.props.donnerCallback()
    }

    viewClicked = () =>  {

        this.props.moreOptionCallback()
        let { goToViewJobPage} = this.props

        goToViewJobPage()
    }

    editClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Edit Clicked!")
    }

    copyClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Copy Clicked!")
    }

    closeClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Close Clicked!")
    }

    deleteClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Delete Clicked!")
    }

    republishClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Republish Clicked!")
    }

    publishClicked = () =>  {
        this.props.moreOptionCallback()
        alert("Publish Clicked!")
    }

    info(byName) {
        if (byName == "Active") {
            return {
                color: "custom-green",
                more: ["View", "Edit", "Copy", "Close", "Delete"],
                moreCallbacks: [() => this.viewClicked(), () => this.editClicked(), () => this.copyClicked(), () => this.closeClicked(), () => this.deleteClicked()]

            }
        }

        if (byName == "Closed") {
            return {
                color: "custom-grey",
                more: ["View", "Edit", "Copy", "Republish", "Delete"],
                moreCallbacks: [() => this.viewClicked(), () => this.editClicked(), () => this.copyClicked(), () => this.republishClicked(), () => this.deleteClicked()]
            }
        }

        if (byName == "Draft") {
            return {
                color: "custom-blue",
                more: ["View", "Edit", "Copy", "Publish", "Delete"],
                moreCallbacks: [() => this.viewClicked(), () => this.editClicked(), () => this.copyClicked(), () => this.publishClicked(), () => this.deleteClicked()]
            }
        }

        return ""

    }



    render() {

        let card = this.props.card;

        const badge = this.info(card.badge.text)
        const listOfItems = badge.more.map((item, index) => <DropdownItem className="dropdown-item" key={index} onClick={badge.moreCallbacks[index]}>{item}</DropdownItem>);


        return (
            <div>
                <Container className="ml-0 pt-3 pb-3 pr-3">
                    <Row>
                        <Col lg="7" md="6" sm="7" xs="7">
                            <span className="card-title-text">{card.name}</span>
                        </Col>
                        <Col lg="3" md="5" sm="4" xs="3" className="text-right pr-0">
                            <Badge color="success" id="statusButton" className= {"pt-2 pb-2 pl-3 pr-3 mr-m5 " + badge.color} pill>{card.badge.text}</Badge>
                        </Col>
                        <Col lg="2" md="1" sm="1" xs="2" className="pl-0 text-right">
                            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} onClick={this.donerClicked}>

                                <DropdownToggle id="smallCardDropdown" color="" className="transparent-button disable-border">
                                <img src={doner} alt="..." className="doner"/>
                                </DropdownToggle>

                                 <DropdownMenu className="dropdown-menu">
                                     {listOfItems}
                                </DropdownMenu>

                            </Dropdown>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <span className="middle-card-text">{card.place} &middot; {card.location}</span>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <span className="salary">&pound;{card.salary.from} - &pound;{card.salary.to}</span>
                        </Col>
                    </Row>
                    <Row className="mb-2">
                        <Col>
                            <span className="date">{card.date}</span>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}

export default  connect(mapStateToProps, {goToViewJobPage: () => push("/jobs/view")})(JobSmallCard);