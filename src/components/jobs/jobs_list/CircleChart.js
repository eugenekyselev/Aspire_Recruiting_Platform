import React, { Component } from 'react'
import {Doughnut} from 'react-chartjs-2';

class CircleChart extends Component {

    render() {

        const data = {
            labels: [],
                datasets: [{
                label: this.props.name,
                data: [this.props.percentage, 100-this.props.percentage],
                backgroundColor: [
                    '#666666', "rgb(249, 249, 249)"
                ],
                borderColor: [
                    'transparent'
                ],
                borderWidth: 2
            }]
        }

        const Chart = () => <div className="chart">
            <Doughnut data={data} height={this.props.height} width={this.props.width} options={{
                cutoutPercentage: '60',
                rotation: (Math.PI * 3.0 / 2.0),
                animation: {
                    animateRotate: this.props.animated
                },
                layout: {
                    padding: 0
                },
                legend: {
                    display: false,
                },
                tooltips: {
                    enabled: false
                }

            }}/>
        </div>

        return (
            <div className="chartContainer">
                <Chart/>
                <div className="ten-pixels-empty-div"></div>
                <div className="chartText">
                    <span className="percentage-text chart-text-span">{this.props.percentage}%</span>
                    <span className="chart-name chart-text-span">{this.props.name}</span>
                </div>
            </div>
        );
    }
    };

export default CircleChart;