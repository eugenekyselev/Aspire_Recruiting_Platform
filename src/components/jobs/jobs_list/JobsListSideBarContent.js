import React, { Component } from 'react'
import leftEnabledArrow from "../../../assets/img/jobs_list/left-enabled-arrow.svg"
import {Container} from "reactstrap"
import classnames from "classnames";
class JobsListSideBarContent extends Component {

    constructor(props) {
        super(props);

        this.moved = this.moved.bind(this);


    }

    moved(event) {
        event.stopPropagation();
        return false
    }
    
    render() {
        let {content, counters} = this.props
        console.log("this.props.selectedState : "+this.props.selectedState )
        return (
            <div className="vw100" onTouchMove={this.moved} onDragStart={this.moved} onMouseDown={this.moved}>
                <Container  className="mb-3">
                <button draggable="false" onClick={this.props.backClicked} className="pt-3 pb-3 background-transparent"><img src={leftEnabledArrow} alt="..."/></button>
                <div draggable="false" className={classnames({ "link-in-selected-state": this.props.selectedState === 'best'}, "d-block", "pb-2", "link-style-preciate")} onClick={() => this.props.applicantsFilterClicked("best")}>Best Matching ({counters.best})</div>
                <div draggable="false" className={classnames({ "link-in-selected-state": this.props.selectedState === 'applicant'},"d-block", "pb-2", "link-style-preciate")} onClick={() => this.props.applicantsFilterClicked("applicant")}>Applicants ({counters.applicants})</div>
                <div draggable="false" className={classnames({ "link-in-selected-state": this.props.selectedState === 'interviewing'},"d-block", "pb-2", "link-style-preciate")} onClick={() => this.props.applicantsFilterClicked("interviewing")}>Interviewing ({counters.interviewing})</div>
                <div draggable="false" className={classnames({ "link-in-selected-state": this.props.selectedState === 'declined'}, "d-block", "pb-2", "link-style-preciate")} onClick={() => this.props.applicantsFilterClicked("declined")}>Declined ({counters.declined})</div>
                {content}
                </Container>
            </div>
        );
    }
};

export default JobsListSideBarContent;