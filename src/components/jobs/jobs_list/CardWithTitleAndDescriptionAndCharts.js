import React, { Component } from 'react'
import { Container, Row, Col, NavLink, Badge} from 'reactstrap';
import {Link} from "react-router-dom";
import CircleChart from './CircleChart';

class CardWithTitleAndDescriptionAndCharts extends React.Component {

    info(byName) {

        if (byName == "none") {
            return {
                colorClass: "border-transparent",
                text: ""

            }
        }

        if (byName == "applied") {
            return {
                colorClass: 'border-blue',
                text: "Applied"
            }
        }

        if (byName == "interviewing") {
            return {
                colorClass: 'border-yellow',
                text: "Interviewing"
            }
        }

        if (byName == "hired") {
            return {
                colorClass: "border-green",
                text: "Hired"
            }
        }

        return ""

    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.shouldRerender;
    }

    render() {
        let card = this.props.card;

        const result = this.info(this.props.type);

        return (
            <div>
                <Container className="pl-0 pt-2 pb-2 pr-3">
                        <div className="title-container">
                            <h5><span className="card-title-text self-left ">{card.name}</span></h5>
                            <Badge color="light" className={"self-right badge-item border " + result.colorClass} pill>{result.text}</Badge>
                        </div>
                    <Row>
                        <Col>
                            <p><span className="middle-card-text">{card.description}</span></p>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <NavLink className="invite-card-underscored-text pl-0" tag={Link} to={card.ref}><ins>View profile</ins></NavLink>
                        </Col>
                    </Row>
                    <div className="flex-div left-offset">
                        <CircleChart animated={this.props.animated} className="chart-scale-class" width={this.props.chartWidth} height={this.props.chartHeight} percentage={this.props.percentage.skills} name="Skills"/>
                        <div  className="thirty-pixels-empty-div"/>
                        <CircleChart animated={this.props.animated} className="chart-scale-class" width={this.props.chartWidth} height={this.props.chartHeight} percentage={this.props.percentage.culture} name="Culture"/>
                        <div className="thirty-pixels-empty-div"/>
                        <CircleChart animated={this.props.animated} className="chart-scale-class" width={this.props.chartWidth} height={this.props.chartHeight} percentage={this.props.percentage.values} name="Values"/>
                    </div>
                </Container>
            </div>
        );
    }
};

CardWithTitleAndDescriptionAndCharts.defaultProps = {
    shouldRerender : true
}

export default CardWithTitleAndDescriptionAndCharts;