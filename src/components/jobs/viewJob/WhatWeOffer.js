import React from 'react';
import { Component } from 'react';

class WhatWeOffer extends Component {

    render() {

        return (
            <div>
                <p><span className="bold-font">What we offer</span></p>
                <p><span className="small-bold">Work-Life Balance</span></p>
                <div className="grid-container">
                    <button>flexible working hours</button>
                    <button>children creche</button>
                    <button>on-site childcare</button>
                </div>
                <p><span className="small-bold">Compensation</span></p>
                <div className="grid-container">
                    <button>individual / company performance based bonus scheme</button>
                    <button>affiliate scheme</button>
                </div>
                <p><span className="small-bold">Environment & Community</span></p>
                <div className="grid-container">
                    <button>active apprentice program</button>
                    <button>engagement with selected charities</button>
                    <button>environmentally friendly programs</button>
                </div>
                <p><span className="small-bold">Wealth & Health</span></p>
                <div className="grid-container">
                    <button>gym membership</button>
                    <button>free fruit in the office</button>
                </div>
                <p><span className="small-bold">Development & Growth</span></p>
                <div className="grid-container">
                    <button>on-going training programs</button>
                </div>
            </div>

        );
    }
};

export default WhatWeOffer;