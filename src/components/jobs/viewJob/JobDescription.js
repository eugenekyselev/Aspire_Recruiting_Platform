import React from 'react';
import { Component } from 'react';

class JobDescription extends Component {

    render() {

        return (
            <div>
                <p><span className="bold-font">Job Description</span></p>
                <p><span className="normal-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>
                <p><span className="small-bold">Short description of the role</span></p>
                <p><span className="big-line-space">First and foremost, you are an outstanding writer, who excels at storytelling. You have had two or more years of proofreading and editing experience, preferably in a mission-driven environment.</span></p>
                <p><span className="small-bold">Daily responsibilities will include:</span></p>
                <p><span className="big-line-space">Report, write, and edit engaging written content—inspirational stories about schools, knowledge sharing, opinion pieces, marketing/sales materials</span></p>
                <p><span className="small-bold">Candidate Requirements</span></p>
                <table className="summary">
                    <tbody>
                    <tr>
                        <td className="small-text">Education</td>
                        <td className="small-text">Seniority level</td>
                        <td className="small-text">Years of experiance</td>
                    </tr>
                    <tr>
                        <td className="table-text">Master's degree</td>
                        <td className="table-text">Mid-Senior level</td>
                        <td className="table-text">3–5 years</td>
                    </tr>
                    </tbody>

                </table>
                <p><span className="small-bold">Skills</span></p>
                <ul>
                    <li>Journalism</li>
                    <li>College degree in English</li>
                    <li>Advertising experience</li>
                    <li>English grammar and usage</li>
                    <li>Verbal communication</li>
                    <li>Strong decision-making skills</li>
                    <li>Ability to manage multiple projects</li>
                    <li>Time-management</li>
                </ul>
            </div>
        );
    }
};

export default JobDescription;