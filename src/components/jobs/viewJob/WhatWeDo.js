import React from 'react';
import { Component } from 'react';
import {
    Button
} from 'reactstrap';

class WhatWeDo extends Component {

    render() {

        return (
            <div>
                <span className="bold-font">What we do</span>
                <div className="message-info-container">
                    <div className="avatar-for-company"></div>
                    <p><span className="contact-name"><br/>Steve Morales</span></p>
                    <p><span>HR Manager</span></p>
                    <Button className="dark-button btn-dark">Message</Button>
                </div>
                <p>
                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
<br/><br/>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                </p>
                <span className="bold-font">Why we do what we do</span>
                <p>
                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                </p>
                <span className="bold-font">We are proud of…</span>
                <p>
                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span>
                </p>
            </div>
        );
    }
};

export default WhatWeDo;