import React from 'react';
import { Component } from 'react';
import { Card,
    Col,
    Row,
    Container,
    Button
    } from 'reactstrap';

class JobHeader extends Component {

    render() {
        return (
            <div>
                    <span className="bold-text">Editor</span>
                    <div className="avatar-for-job"></div>
                <p>
                    <span className="small-bold-text">&pound;50 000 - &pound;75 000 per year</span>
                </p>
                <p>
                    <span>
                        <span className="underline-text">SHS Sales and Marketing</span>
                        <span className="oval"></span>
                        <span className="normal-text">Washington</span>
                        <span className="oval"></span>
                        <span className="normal-text">Full-time</span>
                    </span>
                </p>
                <p>
                    <span className="thin-text">Posted a minute ago</span>
                </p>
                <div className="flex-buttons">
                    <Button className="buttons btn-light white-button">Save</Button>
                    <Button className="buttons btn-dark dark-button">Apply</Button>
                </div>
            </div>
        );
    }
};

export default JobHeader;