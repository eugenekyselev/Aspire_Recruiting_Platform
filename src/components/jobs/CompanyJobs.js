import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Col, 
    Row, 
    Container,
    Button  } from 'reactstrap';

class CompanyJobs extends Component {

    render() {

        return (
            <Container>
                <Row className="mt-4">
                    <Col>
                        <h4>Jobs</h4>
                        <ul className="list-unstyled username">
                            <li><small>You don't have any job yet.</small></li>
                            <li><small>Start posting by clicking button below.</small></li>
                        </ul>
                        <Button color="primary" className="m-2" size="lg" tag={Link} to="/jobs/create">Create job</Button>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default connect(null, {})(CompanyJobs)
