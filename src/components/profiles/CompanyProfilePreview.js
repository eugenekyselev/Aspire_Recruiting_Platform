import React, {Component} from 'react';
import {
    Card,
    Col,
    Row,
    Container,
    Nav,
    NavItem
} from 'reactstrap';

import star from "../../assets/img/shape/star.png"
import twitter from "../../assets/img/social/twitter.svg"
import facebook from "../../assets/img/social/facebook.svg"
import linkedin from "../../assets/img/social/linkedin.svg"

import arrowGray from "../../assets/img/shape/arrow-gray.png"
import arrowDark from "../../assets/img/shape/arrow-dark.png"

class CompanyProfilePreview extends Component {




    componentDidMount() {

    }




    render() {

        console.log(this.state);

        return (
            <div className={'company-preview'}>
                <div className={'company-preview-bg-img'}>
                    <div className={'company-preview-avatar-img'}> </div>
                </div>
                <div className={'company-preview-jobs'}>
                    <div className = {'company-preview-jobs-text'}> Company jobs </div>
                </div>
                <div className={'company-preview-name'}> SHS Sales  and Marketing</div>
                <div className={'company-preview-name-description'}>Marketing and Advertising </div>


                <div className={'company-preview-info'}>
                    <Row>
                        <div className={'block'}>
                            <div className={'company-preview-info-block'}> Number of employees </div>
                            <div className={'company-preview-info-value'}>50-100 </div>
                        </div>

                        <div className={'block'}>
                            <div className={'company-preview-info-block'}> Phone</div>
                            <div className={'company-preview-info-value'}> +44 (0) 1452 378500 </div>
                        </div>

                        <div className={'block'}>
                            <div className={'company-preview-info-block'}> Headquaters</div>
                            <div className={'company-preview-info-value'}> 199 Airport Road West, Belfast, BT3 9ED </div>
                        </div>
                        <div className={'social-icons'}>
                            <Row>
                                <div className={'logo'}> </div>
                                <div className={'logo'}>
                                    <img src={linkedin} className={'social-logo'} alt={'logo'}/>
                                </div>
                                <div className={'logo'}>
                                    <img src={facebook} className={'social-logo'} alt={'logo'}/>
                                </div>
                                <div className={'logo'}>
                                    <img src={twitter} className={'social-logo'} alt={'logo'}/>
                                </div>
                            </Row>


                        </div>
                    </Row>
                </div>


                <Row>
                    <div className={'preview-main'}>
                        <div className={'company-preview-whatWeDo'}>
                            <h1> What we do</h1>
                            <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                            <h1> Why we do what we do </h1>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                            <h1>We are proud of</h1>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>

                        <div className={'company-preview-background'}>
                            <h1> Perks and Benefits </h1>
                            <h2> Work-Life Balance </h2>
                            <div>
                                <Row>
                                    <div className={'company-preview-perks'}>
                                        <span>flexible working hours </span>
                                    </div>
                                    <div className={'company-preview-perks'}>
                                        <span>children creche</span>

                                    </div>
                                    <div className={'company-preview-perks'}>
                                        <span>on-site childcare</span>
                                    </div>
                                </Row>
                            </div>
                            <div className={'company-preview-perks-compensation'}>
                                <h2>Compensation</h2>
                                <div>
                                    <Row>
                                        <div className={'company-preview-perks'}>
                                            <span>individual / company performance based bonus scheme</span>
                                        </div>
                                        <div className={'company-preview-perks'}>
                                            <span>affiliate scheme</span>
                                        </div>
                                    </Row>
                                </div>
                            </div>
                            <div className={'company-preview-perks-environment'}>
                                <h2>Environment & Community</h2>
                                <div>
                                    <Row>
                                        <div className={'company-preview-perks'}>
                                            <span>active apprentice program</span>
                                        </div>
                                        <div className={'company-preview-perks'}>
                                            <span>engagement with selected charities</span>
                                        </div>
                                        <div className={'company-preview-perks'}>
                                            <span>environmentally friendly programs</span>
                                        </div>
                                    </Row>

                                </div>
                            </div>
                            <div className={'company-preview-perks-wealth'}>
                                <h2>Wealth & Health</h2>
                                <div>
                                    <Row>
                                        <div className={'company-preview-perks'}>
                                            <span>gym membership </span>
                                        </div>
                                        <div className={'company-preview-perks'}>
                                            <span>free fruit in the office</span>
                                        </div>
                                    </Row>
                                </div>
                            </div>
                            <div className={'company-preview-perks-development'}>
                                <h2>Development & Growth</h2>
                                <div>
                                    <Row>
                                        <div className={'company-preview-perks'}>
                                            <span>on-going training programs</span>
                                        </div>
                                    </Row>
                                </div>
                            </div>
                        </div>
                        <div className={'company-preview-jobs-background'}>
                            <div className={'company-preview-jobs-background-companyjobs'}>CompanyJobs
                                <a href={'#all'}> See all jobs</a>
                            </div>
                            <Row>
                                <div className={'company-preview-jobs-block'}>
                                    <div className={'editor'}>Editor</div>
                                    <span className={'text-1'}>Forbes</span>
                                    <span className={'oval'}></span>
                                    <span className={'text-1'}>Washington</span>
                                    <div className={'text-2'} >&pound;50 000 - &pound;75 000 per year</div>
                                    <div className={'text-content'}>
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit, sed do eiusmod tempor.
                                    </div>
                                    <button className={'button-apply'}>Apply job</button>
                                    <img src={star}  className="shape" alt="shape"/>
                                </div>

                                <div className={'company-preview-jobs-block'}>
                                    <div className={'editor'}>Editor</div>
                                    <span className={'text-1'}>Forbes</span>
                                    <span className={'oval'}></span>
                                    <span className={'text-1'}>Washington</span>
                                    <div className={'text-2'} >&pound;50 000 - &pound;75 000 per year</div>
                                    <div className={'text-content'}>
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit, sed do eiusmod tempor.
                                    </div>
                                    <button className={'button-apply'}>Apply job</button>
                                    <img src={star}  className="star" alt="star"/>
                                </div>
                            </Row>
                        </div>
                        <div className={'company-preview-our-office-background'}>
                            <Row>
                                <div className={'header-text'}>Our Office</div>
                                <img src={arrowGray} className={'arrow-gray'} alt={'arr'}/>
                                <img src={arrowDark} className={'arrow-dark'} alt={'arr'}/>

                            </Row>
                            <Row>
                                <div className={'photo-company'}></div>
                                <div className={'photo-company'}></div>
                                <div className={'photo-company'}></div>

                            </Row>

                            <Row className={'our-team'}>
                                <div className={'header-text'}>Our Office</div>
                                <img src={arrowGray} className={'arrow-gray'} alt={'arr'}/>
                                <img src={arrowDark} className={'arrow-dark'} alt={'arr'}/>

                            </Row>
                            <Row>
                                <div>
                                    <div className={'photo-company'}></div>
                                    <div>
                                        <div className={'who'}>Glen Walker</div>
                                        <div className={'description'}>CEO</div>
                                    </div>
                                </div>
                                <div>
                                    <div className={'photo-company'}></div>
                                    <div>
                                        <div className={'who'}>Steve Morales</div>
                                        <div className={'description'}>Head of HR Department</div>
                                    </div>
                                </div>
                                <div>
                                    <div className={'photo-company'}></div>
                                    <div>
                                        <div className={'who'}>Dylan Edwards</div>
                                        <div className={'description'}>Head of Sales Department</div>
                                    </div>
                                </div>
                            </Row>
                        </div>
                    </div>
                    <div>
                        <div className={'company-representative-background'}>
                            <div className={'company-representative-header'}>Company Representative</div>
                            <div className={'company-representative-img'}></div>
                            <div>
                                <div className={'who'}>Steve Morales</div>
                                <div className={'description'}>HR manager</div>
                                <button className={'company-representative-btn'}>Message</button>

                            </div>
                        </div>
                        <div className={'company-culture-background'}>
                            <div className={'company-culture-header'}>Company Culture</div>
                            <div className={'company-culture-content'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                        </div>
                    </div>
                </Row>

            </div>






        );
    }
}

// class ProfileCircle extends Component{
//     render() {
//         return (
//
//         )
//     }
// }

export default CompanyProfilePreview;
