import React, { Component } from 'react';
import { Card, Col, Row, Container, CardHeader, CardBody, Table } from 'reactstrap';
import { connect } from 'react-redux'

class AdminProfile extends Component {

    render() {

        let { profile } = this.props;
    
        return (
            <Container>
                <Row className="mt-4">
                    <Col md={8} lg={8}>
                        <Card className="mb-4">
                            <CardHeader>Profile ({profile.role})</CardHeader>
                            <CardBody>        
                                <Table>
                                    <tbody>
                                        <tr>
                                            <th scope="row">First Name:</th>
                                            <td>{profile.firstname}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Email:</th>
                                            <td>{profile.email}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default connect(null, null)(AdminProfile);
