import React, { Component } from 'react';

import CompanyProfileForm from '../forms/profile/CompanyProfileForm'

class CompanyProfile extends Component {

    submit = (values) => {

        console.log(values)

        alert("Profile: "+JSON.stringify(values));
    }

    render() {

        return (
            <CompanyProfileForm onSubmit={this.submit} />
        );
    }
}

export default CompanyProfile;
