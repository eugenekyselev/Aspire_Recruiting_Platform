import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper'
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import connectedAuthWrapper from 'redux-auth-wrapper/connectedAuthWrapper'
// import Loading from '../components/layout/Loading'
import { replace } from 'connected-react-router'
import { compose } from 'redux'

const locationHelper = locationHelperBuilder({});

export const UserIsAuthenticated = connectedRouterRedirect({
    wrapperDisplayName: 'UserIsAuthenticated',
    // AuthenticatingComponent: Loading,
    allowRedirectBack: true,
    redirectPath: (state, ownProps) =>
        locationHelper.getRedirectQueryParam(ownProps) || '/login',
    // authenticatingSelector: user => user.data === null,
    authenticatedSelector: state => state.user.data !== null,
    redirectAction: newLoc => (dispatch) => {
        dispatch(replace(newLoc));
    }
});

const UserIsCompany = connectedRouterRedirect({
    wrapperDisplayName: 'UserIsCompany',
    allowRedirectBack: false,
    redirectPath: (state, ownProps) =>
        locationHelper.getRedirectQueryParam(ownProps) || '/profile',
    authenticatedSelector: state => state.user.role === "company",
    redirectAction: newLoc => (dispatch) => {
        dispatch(replace(newLoc));
    },
});

export const userIsCompanyChain = compose(UserIsAuthenticated, UserIsCompany)

export const UserIsNotAuthenticated = connectedRouterRedirect({
    wrapperDisplayName: 'UserIsNotAuthenticated',
    // AuthenticatingComponent: Loading,
    allowRedirectBack: false,
    redirectPath: (state, ownProps) =>
        locationHelper.getRedirectQueryParam(ownProps) || '/profile',
    // authenticatingSelector: user => user.data === null,
    authenticatedSelector: state => state.user.data === null,
    redirectAction: newLoc => (dispatch) => {
        dispatch(replace(newLoc));
    },
});

export const VisibleOnlyAuth = connectedAuthWrapper({
    authenticatedSelector: state => state.user.data !== null,
    wrapperDisplayName: 'VisibleOnlyAuth'
})

export const HiddenOnlyAuth = connectedAuthWrapper({
    authenticatedSelector: state => state.user.data === null,
    wrapperDisplayName: 'HiddenOnlyAuth'
})

export const VisibleOnlyCompany = connectedAuthWrapper({
    authenticatedSelector: state => state.user.data !== null && state.user.role === "company",
    wrapperDisplayName: 'VisibleOnlyCompany'
})

export const VisibleOnlyCandidate = connectedAuthWrapper({
    authenticatedSelector: state => state.user.data !== null && state.user.role === "candidate",
    wrapperDisplayName: 'VisibleOnlyCandidate'
})

export const VisibleOnlyAdmin = connectedAuthWrapper({
    authenticatedSelector: state => state.user.data !== null && state.user.role === "admin",
    wrapperDisplayName: 'VisibleOnlyAdmin'
})