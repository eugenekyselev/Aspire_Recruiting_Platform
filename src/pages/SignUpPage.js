import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container,
    Row,
    Col,
    Card,
    TabContent, 
    TabPane, 
    Nav, 
    NavItem, 
    NavLink } from 'reactstrap';
import classnames from 'classnames';
import logo from '../assets/img/aspire-logo.svg'
import CompanySignUpForm from '../components/forms/signup/CompanySignUpForm';
import CandidateSignUpForm from '../components/forms/signup/CandidateSignUpForm';

class SignUpPage extends Component {

    constructor(props) {
        super(props);

        let type = "company";

        if (props.location.state !== undefined && props.location.state.type !== undefined){
            type = props.location.state.type;
        }

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: type
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {

        let { activeTab } = this.state

        return (
            <section className="signup-block">
                <Container>
                    <Row className="justify-content-center align-items-center">
                        <Col className="text-center">
                            <Link to="/">
                                <img src={logo} className="aspire-logo" alt="logo"/>
                            </Link>
                        </Col>
                    </Row>
                    <Row className="justify-content-center align-items-center">
                        <Col md={6} lg={5}>
                            <Card>
                                <Nav pills>
                                    <NavItem className={classnames({ active: activeTab === 'company' })}>
                                        <NavLink
                                            onClick={() => { this.toggle('company'); }}>
                                            I'am Enterprise
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className={classnames({ active: activeTab === 'candidate' })}>
                                        <NavLink
                                            onClick={() => { this.toggle('candidate'); }}>
                                            I'am Candidate
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={activeTab}>
                                    <TabPane tabId="company">
                                        <Row>
                                            <Col sm="12">
                                                <CompanySignUpForm />
                                            </Col>
                                        </Row>
                                    </TabPane>
                                    <TabPane tabId="candidate">
                                        <Row>
                                            <Col sm="12">
                                                <CandidateSignUpForm />
                                            </Col>
                                        </Row>
                                    </TabPane>
                                </TabContent>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }
}


export default SignUpPage;
