import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container,
    Row,
    Col,
    Card
} from 'reactstrap';
import logo from '../assets/img/aspire-logo.svg'
import ResetPasswordForm from '../components/forms/resetPassword/ResetPasswordForm';

class ResetPasswordPage extends Component {

    render() {

        return (
            <section className="reset-password-block">
                <Container>
                    <Row className="justify-content-center align-items-center">
                        <Col className="text-center">
                            <Link to="/">
                                <img src={logo} className="aspire-logo" alt="logo"/>
                            </Link>
                        </Col>
                    </Row>
                    <Row className="justify-content-center align-items-center">
                        <Col md={6} lg={5}>
                            <Card>
                                <Row>
                                    <Col sm="12"> 
                                        <ResetPasswordForm />
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }
}

export default ResetPasswordPage;