import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container,
    Card, CardBody, CardTitle, Row, Col, Button} from 'reactstrap';
import NavigationBar from '../../components/layout/NavigationBar';
import JobSmallCard from '../../components/jobs/jobs_list/JobSmallCard';
import InviteToApplyJobCard from '../../components/jobs/jobs_list/InviteToApplyJobCard';
import PaginationComponent from "../../components/layout/PaginationComponent";
import SidebarPreciate from "../../components/layout/SidebarPreciate";
import JobsListSideBarContent from "../../components/jobs/jobs_list/JobsListSideBarContent";
import classnames from 'classnames';

export class JobsListPage extends Component {

    smallCardsDataSource = () => {
        return ([{state: "draft", id: 'a', badge: { text: "Draft"}, name: "Editor 1", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'b', badge: {text: "Active"}, name: "Senior Editor 2", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'c', badge: {text: "Closed"}, name: "Representer 3", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'd', badge: {text: "Draft"}, name: "Editor 4", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'e', badge: {text: "Active"}, name: "Senior Editor 61", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'f', badge: {text: "Closed"}, name: "Representer 51", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'g', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'h', badge: {text: "Active"}, name: "Senior Editor 41", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'i', badge: {text: "Closed"}, name: "Representer 21", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'j', badge: {text: "Draft"}, name: "Editor 23", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'k', badge: {text: "Active"}, name: "Senior Editor 87", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'l', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'm', badge: {text: "Draft"}, name: "Editor", "place 34": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'n', badge: {text: "Active"}, name: "Senior Editor 7", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'o', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'p', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'q', badge: {text: "Active"}, name: "Senior Editor 6", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'r', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 's', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 't', badge: {text: "Active"}, name: "Senior Editor", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'u', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'v', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'w', badge: {text: "Active"}, name: "Senior Editor 5 ", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'x', badge: {text: "Closed"}, name: "Representer 45", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'y', badge: {text: "Draft"}, name: "Editor 71", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'z', badge: {text: "Active"}, name: "Senior Editor 711", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'aa', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'ab', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'ac', badge: {text: "Active"}, name: "Senior Editor 34", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'ad', badge: {text: "Closed"}, name: "Representer 23", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'a', badge: { text: "Draft"}, name: "Editor 1", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'b', badge: {text: "Active"}, name: "Senior Editor 2", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'c', badge: {text: "Closed"}, name: "Representer 3", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'd', badge: {text: "Draft"}, name: "Editor 4", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'e', badge: {text: "Active"}, name: "Senior Editor 61", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'f', badge: {text: "Closed"}, name: "Representer 51", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'g', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'h', badge: {text: "Active"}, name: "Senior Editor 41", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'i', badge: {text: "Closed"}, name: "Representer 21", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'j', badge: {text: "Draft"}, name: "Editor 23", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'k', badge: {text: "Active"}, name: "Senior Editor 87", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'l', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'm', badge: {text: "Draft"}, name: "Editor", "place 34": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'n', badge: {text: "Active"}, name: "Senior Editor 7", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'o', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'p', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'q', badge: {text: "Active"}, name: "Senior Editor 6", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'r', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 's', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 't', badge: {text: "Active"}, name: "Senior Editor", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'u', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'v', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'w', badge: {text: "Active"}, name: "Senior Editor 5 ", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'x', badge: {text: "Closed"}, name: "Representer 45", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'y', badge: {text: "Draft"}, name: "Editor 71", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'z', badge: {text: "Active"}, name: "Senior Editor 711", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'aa', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'ab', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'ac', badge: {text: "Active"}, name: "Senior Editor 34", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'ad', badge: {text: "Closed"}, name: "Representer 23", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'a', badge: { text: "Draft"}, name: "Editor 1", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'b', badge: {text: "Active"}, name: "Senior Editor 2", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'c', badge: {text: "Closed"}, name: "Representer 3", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'd', badge: {text: "Draft"}, name: "Editor 4", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'e', badge: {text: "Active"}, name: "Senior Editor 61", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'f', badge: {text: "Closed"}, name: "Representer 51", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'g', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'h', badge: {text: "Active"}, name: "Senior Editor 41", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'i', badge: {text: "Closed"}, name: "Representer 21", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'j', badge: {text: "Draft"}, name: "Editor 23", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'k', badge: {text: "Active"}, name: "Senior Editor 87", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'l', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'm', badge: {text: "Draft"}, name: "Editor", "place 34": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'n', badge: {text: "Active"}, name: "Senior Editor 7", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'o', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'p', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'q', badge: {text: "Active"}, name: "Senior Editor 6", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'r', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 's', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 't', badge: {text: "Active"}, name: "Senior Editor", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'u', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'v', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'w', badge: {text: "Active"}, name: "Senior Editor 5 ", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'x', badge: {text: "Closed"}, name: "Representer 45", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'y', badge: {text: "Draft"}, name: "Editor 71", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'z', badge: {text: "Active"}, name: "Senior Editor 711", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'aa', badge: {text: "Closed"}, name: "Representer", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"},
            {state: "draft", id: 'ab', badge: {text: "Draft"}, name: "Editor", "place": "Forbes", "location": "Washington", salary: {"from": "50 000", "to": "75 000"}, "date": "15 Sep 2018"},
            {state: "active", id: 'ac', badge: {text: "Active"}, name: "Senior Editor 34", "place": "JQ", "location": "Riga", salary: {"from": "150 000", "to": "275 000"}, "date": "10 Sep 2017"},
            {state: "closed", id: 'ad', badge: {text: "Closed"}, name: "Representer 23", "place": "Times", "location": "Berlin", salary: {"from": "25 000", "to": "105 000"}, "date": "5 May 2018"}])
    }

     bigCards = [{favourite: true, user_id: "a1", state: "best", job_id:"a", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, state: "best", "job_id":"b","name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, user_id: "i9", state: "best", "job_id":"a", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: true, user_id: "b2", state: "applicant", job_id:"d", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, state: "applicant", "job_id":"e", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 10, values: 95}}, {favourite: true, user_id: "j10", state: "applicant", "job_id":"a", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 81, culture: 10, values: 91}}, {favourite: false, user_id: "r17", state: "best", job_id:"g", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "x23", state: "best", "job_id":"h", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "d44", state: "interviewing", "job_id":"i", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: false, user_id: "c3", state: "interviewing", job_id:"j", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, state: "interviewing", "job_id":"a", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 50, culture: 90, values: 15}}, {favourite: true, user_id: "k11",  state: "interviewing", "job_id":"v", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 82, culture: 20, values: 25}}, {favourite: false, user_id: "s18", state: "applicant", job_id:"ab", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "y24", state: "best", "job_id":"c", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "e55", state: "interviewing", job_id: "a", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: false, user_id: "d4", state: "declined", job_id:"k", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, state: "declined", "job_id":"a", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 10, culture: 90, values: 95}}, {favourite: true, user_id: "l12", state: "declined", "job_id":"w", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 83, culture: 30, values: 94}}, {favourite: false, user_id: "t19", state: "interviewing", "job_id":"ac", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "z25", state: "best", "job_id":"d", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "f66", job_id: "b" , state: "interviewing", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: true, user_id: "e5", state: "best", job_id:"a", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, state: "best", "job_id":"a", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 83, culture: 90, values: 95}}, {user_id: "m13", state: "applicant", "job_id":"a", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 84, culture: 40, values: 55}}, {favourite: false, user_id: "u20", state: "declined", job_id:"ad", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "a11", state: "interviewing", "job_id":"e", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "g77", job_id:"b", state: "best", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: true, user_id: "f6", state: "applicant", job_id:"a", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, state: "applicant", "job_id":"s", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 81, culture: 90, values: 55}}, {favourite: true, user_id: "n14", state: "best", "job_id":"y", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 85, culture: 50, values: 65}}, {favourite: false, user_id: "v21", state: "best", job_id:"a", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "b22", state: "interviewing", "job_id":"f", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "h88", job_id: "v", state: "interviewing", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: false, user_id: "g7", state: "interviewing", job_id:"n", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, state: "interviewing", "job_id":"t", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 83, culture: 90, values: 35}}, {favourite: true, user_id: "o15", state: "interviewing", "job_id":"z", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 86, culture: 60, values: 75}}, {favourite: false, user_id: "w22", state: "best", job_id:"b", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "c33", state: "interviewing", "job_id":"g", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: false, user_id: "i99", job_id:"c", state: "declined",  "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}},
{favourite: true, user_id: "h8", state: "declined", job_id:"o", "name": "Editor", "description": "Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, state: "declined", "job_id":"a", "name": "Middle Editor", "description": "Middle Description", "ref": "/profile", percentage: {skills: 80, culture: 90, values: 95}}, {favourite: true, user_id: "q16", state: "declined", "job_id":"a", "name": "Sinor Editor", "description": "Sinor Description", "ref": "/profile", percentage: {skills: 87, culture: 70, values: 97}}];

    filterCardsByStatus = (array, status) => {return  (array.filter(card => card.state === status))}
    intOrZerro = number => {return (number != undefined ? number : 0)}
    getCounter = (array, status) => {return (this.intOrZerro(this.filterCardsByStatus(array, status).length))}
    computedCounters = bigCards => {return ({"best": this.getCounter(bigCards, 'best'),
         "applicants": this.getCounter(bigCards, 'applicant'),
         "interviewing": this.getCounter(bigCards, 'interviewing'),
         "declined": this.getCounter(bigCards, 'declined')})
     }

    constructor(props) {
        super(props);

        this.jobsTabWasClicked = this.jobsTabWasClicked.bind(this);
        this.applicantsTabWasClicked = this.applicantsTabWasClicked.bind(this);
        this.favouriteClicked = this.favouriteClicked.bind(this);

        this.jobClicked = this.jobClicked.bind(this);
        this.smallCardMoreOptionClickedCallback = this.smallCardMoreOptionClickedCallback.bind(this)

        this.state = {smallCards:[], bigCards:[], counters: this.computedCounters([]), selectedJobsTab: "all", animateCharts: true, shouldOpenSidebar : false, donerWasClicked: false, moreOptionClicked: false}
    }

    componentDidMount() {

        let smallCardsList = this.smallCardsDataSource();
        const bidCardsWithId = this.bigCards.filter(bigCard => smallCardsList[0].id === bigCard.job_id);
        const startCounterts = this.computedCounters(bidCardsWithId);

        this.scrolled = this.scrolled.bind(this);

        this.setState({smallCards: this.props.setup(smallCardsList, 12, "joblist"), bigCards: bidCardsWithId, counters: startCounterts, selectedJobsTab: "all", selectedApplicantsTab: "best"})
    }

    scrolled(event) {
        const mql = window.matchMedia(`(max-width: 768px)`);

        if (!mql) {
            return
        }

        const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
        const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

        let height = document.getElementsByTagName("footer")[0].getBoundingClientRect().height
        if ((window.pageYOffset - windowHeight) > document.getElementsByTagName("footer")[0].getBoundingClientRect().height) {
            window.scroll(0, (documentHeight - height - windowHeight));
        }
    }

    componentWillUnmount() {
        this.setState({shouldOpenSidebar: false});
    }

    jobsTabWasClicked(tabType) {
        this.setState({selectedJobsTab: tabType});
     }

    applicantsTabWasClicked(tabType) {
        this.setState({selectedApplicantsTab: tabType});
     }

     favouriteClicked(newState, id, status) {
        const filtered = this.bigCards.filter(card => card.user_id === id)[0];
        filtered.favourite = newState
     }

     onSidebarBack = () => {
        this.setState({shouldOpenSidebar: false});
     }

    donnerCallback = () => {
        this.setState({donerWasClicked: true});
    }

    smallCardMoreOptionClickedCallback = () => {
        console.log("smallCardMoreOptionClickedCallback")
        this.setState({moreOptionClicked: true});
    }

    jobClicked(id) {
        let donerClicked = this.state.donerWasClicked

        let moreOptionClicked = this.state.moreOptionClicked
        if (donerClicked || moreOptionClicked) {
            this.setState({donerWasClicked: false, moreOptionClicked: false});
            return
        }


        let smallCardsList = this.smallCardsDataSource();
        const bidCardsFilteredList = this.bigCards.filter(bigCard => id === bigCard.job_id);
        this.setState({bigCards: bidCardsFilteredList, counters: this.computedCounters(bidCardsFilteredList), selectedApplicantsTab: "best", shouldOpenSidebar: !donerClicked})
    }

    render() {
        const listOfSmallItems = this.state.smallCards.map((item, index) => {return ((this.state.selectedJobsTab == item.state || this.state.selectedJobsTab == "all") && <Card onClick={() => this.jobClicked(item.id)}  key={item.id} className={classnames({"small-card-settings-active": ([...this.state.bigCards][0] != undefined && "job_id" in [...this.state.bigCards][0]) ? ([...this.state.bigCards][0].job_id === item.id) : false}, "pl-2", "mt-4", "border-0", "small-card-settings", "custom-shadow")}><JobSmallCard moreOptionCallback={this.smallCardMoreOptionClickedCallback} donnerCallback={this.donnerCallback} card={item} /></Card>)});
        const listOfBigCards = this.state.bigCards.map((item, index) => { return ((this.state.selectedApplicantsTab == item.state) && <Card key={index} className="pb-2 pt-2 pl-2 mt-4 shadow border-0 "><InviteToApplyJobCard isInFavourite={item.favourite} favouriteClicked={this.favouriteClicked} animated={this.state.animateCharts} type='none' card={item} /></Card>)});
        const counters = this.state.counters;
        const unique = [...new Set(this.smallCardsDataSource().map(item => item.id))];

        return (<section className="jobs-list">
            <SidebarPreciate shouldOpenSidebar={this.state.shouldOpenSidebar} content={<JobsListSideBarContent counters={counters} content={listOfBigCards} backClicked={this.onSidebarBack} selectedState={this.state.selectedApplicantsTab} applicantsFilterClicked={this.applicantsTabWasClicked}/>}>

            <Container>
                <Card className="border-0 mt-2">
                    <CardBody className="p-0">

                    <CardTitle className="mt-4 mb-4 d-md-inline-block d-sm-inline-block d-inline-block">Jobs</CardTitle>

                        <Button color="primary" className="small-create-job-button float-right rounded d-md-inline-block d-sm-inline-block d-inline-block d-lg-none d-xl-none">Create job</Button>
                        <div className="d-block d-sm-block d-md-block d-lg-none d-xl-none mt-m15">
                            <div className={classnames({ "link-in-selected-state": this.state.selectedJobsTab === 'all'}, 'd-block', 'link-style-preciate')} onClick={() => this.jobsTabWasClicked("all")}>All Jobs</div>
                            <div className={classnames({ "link-in-selected-state": this.state.selectedJobsTab === 'active'}, 'd-block', "pt-2", "link-style-preciate")} onClick={() => this.jobsTabWasClicked("active")}>Active</div>
                            <div className={classnames({ "link-in-selected-state": this.state.selectedJobsTab === 'draft'}, "d-block", "pt-2", "link-style-preciate")} onClick={() => this.jobsTabWasClicked("draft")}>Draft</div>
                            <div className={classnames({ "link-in-selected-state": this.state.selectedJobsTab === 'closed'}, "d-block", "pt-2", "pb-4", "link-style-preciate")} onClick={() => this.jobsTabWasClicked("closed")}>Closed</div>
                        </div>

                    <NavigationBar selectedState={this.state.selectedJobsTab} fontName="main-nav-font" afterClick={this.jobsTabWasClicked} items={[{name:'All Jobs', state: "all"}, {name: 'Active', state: "active"}, {name:'Draft', state:"draft"}, {name:'Closed', state:"closed"}]}/>

                        <Container className="p-0 ">
                            <hr className="col-xs-9 mb-0 mt-m1 mr-50"/>
                            <Button color="primary" className="col-xs-3 float-right create-job rounded d-none d-sm-none d-md-none d-lg-inline-block d-xl-inline-block">Create job</Button>
                        </Container>
                        <Container className="mb-5">
                            <Row >
                                <Col className="pl-0 fixed-i-height pb-6 col-lg-4half" sm="12" xl="5" md="12" xs="12" onScroll={this.scrolled}>
                                    {listOfSmallItems}
                                </Col>
                                <Col xl="7" className="invite-to-apply-card-settings mt-4 pt-3 height-red pre-scrollable d-none d-sm-none d-md-none d-lg-inline-block d-xl-inline-block col-lg-7half">
                                    <NavigationBar fontName="secondary-nav-font" selectedState={this.state.selectedApplicantsTab} afterClick={this.applicantsTabWasClicked} items={[{name: "Best Matching ("+counters.best+")", state: "best"}, {name: 'Applicants ('+counters.applicants+')', state: "applicant"}, {name:'Interviewing ('+counters.interviewing+')', state:"interviewing"}, {name:'Declined ('+counters.declined+')', state:"declined"}]}/>
                                    <hr className="mb-0 mt-0"/>
                                    {listOfBigCards}
                                </Col>
                            </Row>

                         </Container>
                    </CardBody>

                 </Card>
            </Container>
            </SidebarPreciate>
            <Container className="top-minus">
                <Row>
                    <Col></Col>
                    <Col><PaginationComponent name="joblist" maxPagesCount={Math.ceil(unique.length / 6)} /></Col>
                    <Col></Col>
                </Row>
            </Container>
        </section>);
    }
};

export default JobsListPage;