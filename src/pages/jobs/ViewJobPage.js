import React, { Component } from 'react';
import { Card,
    Col,
    Row,
    Container} from 'reactstrap';
import JobHeader from "../../components/jobs/viewJob/JobHeader";
import JobDescription from "../../components/jobs/viewJob/JobDescription";
import WhatWeOffer from "../../components/jobs/viewJob/WhatWeOffer";
import WhatWeDo from "../../components/jobs/viewJob/WhatWeDo";

class ViewJobPage extends Component {

    render() {
        return (
        <section >
            <Container className="view-job">
            <Row className="justify-content-center align-items-center">
                <Col>
                    <Card className="job-header">
                        <JobHeader/>
                    </Card>

                </Col>

            </Row>
            <Row className="justify-content-center align-items-center">
                <Col>
                    <Card className="description">
                        <JobDescription/>
                    </Card>
                </Col>
            </Row>
            <Row className="justify-content-center align-items-center">
                <Col>
                    <Card className="what-we-offer">
                        <WhatWeOffer/>
                    </Card>
                </Col>
            </Row>
            <Row className="justify-content-center align-items-center">
                <Col>
                    <Card className="what-we-do">
                        <WhatWeDo/>
                    </Card>
                </Col>
            </Row>
            </Container>
        </section>
        );
    }
}

export default ViewJobPage;