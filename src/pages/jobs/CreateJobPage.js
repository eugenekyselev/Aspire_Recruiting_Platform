import React, { Component } from 'react';
import { Card, 
    Col, 
    Row, 
    Container,
    Nav, 
    NavItem } from 'reactstrap';
import {
    Link,
    Events,
    scrollSpy
} from 'react-scroll'

class CreateJobPage extends Component {

    componentDidMount () {
        Events.scrollEvent.register('begin', function () {})
        Events.scrollEvent.register('end', function () {})

        scrollSpy.update()
    }

    componentWillUnmount () {
        Events.scrollEvent.remove('begin')
        Events.scrollEvent.remove('end')
    }

    render() {

        return (
            <section className="jobs-block">
                <Container>
                    <h4 className="my-4">Create Post</h4>
                    <Row>
                        <Col xs={12} md={3}>
                            <Nav pills vertical className="sticky-top mb-4">
                                <NavItem>
                                    <Link className="p-3 nav-link"
                                        activeClass="active"
                                        to="job-information"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>
                                        Job Information
                                    </Link>                                  
                                </NavItem>
                                <NavItem>
                                    <Link className="p-3 nav-link"
                                        activeClass="active"
                                        to="job-details"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>
                                        Job Details
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="p-3 nav-link"
                                        activeClass="active"
                                        to="job-description"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>
                                        Job Description
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="p-3 nav-link"
                                        activeClass="active"
                                        to="additional-information"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>
                                        Additional Information
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link className="p-3 nav-link"
                                        activeClass="active"
                                        to="representative"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>
                                        Representative
                                    </Link>
                                </NavItem>
                            </Nav>
                        </Col>
                        <Col xs={12} md={9}>
                            <Card id="job-information" className="mb-4 p-4">
                                <h5>Job Information</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                
                            </Card>
                            <Card id="job-details" className="mb-4 p-4">
                                <h5>Job Details</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                
                            </Card>
                            <Card id="job-description" className="mb-4 p-4">
                                <h5>Job Description</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            
                            </Card>
                            <Card id="additional-information" className="mb-4 p-4">
                                <h5>Additional Information</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            
                            </Card>
                            <Card id="representative" className="mb-4 p-4">
                                <h5>Representative</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>
        );
    }
}

export default CreateJobPage;