import React, { Component } from 'react';
import { connect } from 'react-redux'
import { VisibleOnlyCandidate, VisibleOnlyCompany } from '../../wrappers/index'

import CandidateJobs from '../../components/jobs/CandidateJobs';
import CompanyJobs from '../../components/jobs/CompanyJobs';

class JobsPage extends Component {

    render() {

        const OnlyCompanyBlock = VisibleOnlyCompany(() =>
            <CompanyJobs/>
        );

        const OnlyCandidateBlock = VisibleOnlyCandidate(() =>
            <CandidateJobs/>
        );

        
        return (
            <section className="jobs-block">
                <OnlyCompanyBlock />
                <OnlyCandidateBlock />
            </section>
        );

    }
}

const mapStateToProps = (state) => {
    return {
       
    }
}

export default connect(mapStateToProps, null)(JobsPage);