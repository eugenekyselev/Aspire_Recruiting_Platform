import React from 'react';
import {connect} from "react-redux";
import {Row, Col, Container} from "reactstrap"
import PaginationComponent from '../../components/layout/PaginationComponent'

class PageWithPagination extends React.Component {

    constructor(props) {
        super(props);
        this.setupPagination = this.setupPagination.bind(this);
    }

    setupPagination(forItems, countOnPage, pagename, maxPagesCount) {
        let activePosition = this.props.activePosition.filter(elem => elem.pagename == pagename);
        let start = (activePosition != NaN && activePosition != undefined && activePosition.length > 0) ? activePosition[0].value : 1
        let newStart = (start - 1) * countOnPage < 0 ? 0 : (start - 1) * countOnPage;
        let newFinish = newStart + countOnPage < forItems.length ? newStart + countOnPage : (newStart + (forItems.length - newStart));
        return forItems.slice(newStart, newFinish)
    }

    render() {
        const { children } = this.props;
        const childrenWithProps = React.Children.map(children, child =>
            React.cloneElement(child, {setup: this.setupPagination}));
        return <div>
            {childrenWithProps}
            </div>
    }
}


const mapStateToProps = (state) => {
    return {
        activePosition: state.paginationtReducer.activePosition
    }
}

export default connect(mapStateToProps)(PageWithPagination)
