import React, { Component } from 'react';
import { connect } from 'react-redux'

import { VisibleOnlyCandidate, VisibleOnlyCompany, VisibleOnlyAdmin } from '../wrappers'

import AdminProfile from '../components/profiles/AdminProfile';
import CandidateProfile from '../components/profiles/CandidateProfile';
import CompanyProfile from '../components/profiles/CompanyProfile';

class ProfilePage extends Component {

    render() {


        const OnlyCompanyBlock = VisibleOnlyCompany(() =>
            <CompanyProfile />
        );

        const OnlyCandidateBlock = VisibleOnlyCandidate(() =>
            <CandidateProfile />
        );

        const OnlyAdminBlock = VisibleOnlyAdmin(() =>
            <AdminProfile />
        );

        return (
            <section className="profile-block">
                <OnlyCompanyBlock />
                <OnlyCandidateBlock />
                <OnlyAdminBlock />
            </section>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        
    }
}

export default connect(mapStateToProps, null)(ProfilePage);