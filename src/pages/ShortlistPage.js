import React, { Component } from 'react';
import { Container,
    Row,
    Col,
    Card,
    Input} from 'reactstrap';
import InviteToApllyJobCard from '../components/jobs/jobs_list/InviteToApplyJobCard';
import AcceptDeclinePerson from '../components/shortlist/AcceptDeclinePerson';
import MakeOfferOrDeclinePerson from '../components/shortlist/MakeOfferOrDeclinePerson';
import InviteToApplyHired from  '../components/shortlist/InviteToApplyHired';
import PaginationComponent from "../components/layout/PaginationComponent";
import PageWithPagination from "./base/PageWithPagination";

class ShortlistPage extends Component {

    cardsList = [{user_id: "a1", favourite: true, name: "Editor", description: "Loren ipsum, dolor set amet", ref: "#", type: 'none', percentage: {skills: 33, culture: 34, values: 35}},
        {user_id: "b2", favourite: true, name: "Editor 1", description: "Loren ipsum, dolor set amet", ref: "#", type: 'none', percentage: {skills: 36, culture: 37, values: 38}},
        {user_id: "c3", favourite: false, name: "Editor 2", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 39, culture: 40, values: 41}},
        {user_id: "d4", name: "Editor 3", description: "Loren ipsum, dolor set amet", ref: "#", type: 'hired', percentage: {skills: 80, culture: 90, values: 95}},
        {user_id: "e5", name: "Editor 4", description: "Loren ipsum, dolor set amet", ref: "#", type: 'interviewing', percentage: {skills: 42, culture: 43, values: 44}},
        {user_id: "d6", favourite: false, name: "Editor 5", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 45, culture: 46, values: 47}},
        {user_id: "e7", name: "Editor 6", description: "Loren ipsum, dolor set amet", ref: "#", type: 'interviewing', percentage: {skills: 48, culture: 49, values: 50}},
        {user_id: "f8", favourite: true, name: "Editor 7", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 51, culture: 52, values: 53}},
        {user_id: "g9", favourite: true, name: "Editor 8", description: "Loren ipsum, dolor set amet", ref: "#", type: 'none', percentage: {skills: 54, culture: 55, values: 56}},
        {user_id: "h10", name: "Editor 9", description: "Loren ipsum, dolor set amet", ref: "#", type: 'hired', percentage: {skills: 57, culture: 58, values: 59}},
        {user_id: "i11", favourite: false, name: "Editor 10", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 60, culture: 61, values: 62}},
        {user_id: "j12", favourite: true, name: "Editor 11", description: "Loren ipsum, dolor set amet", ref: "#", type:'none', percentage: {skills: 63, culture: 64, values: 65}},
        {user_id: "k13", favourite: true, name: "Editor 12", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 66, culture: 67, values: 68}},
        {user_id: "l14", favourite: false, name: "Editor 13", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 69, culture: 70, values: 72}},
        {user_id: "m15", favourite: true, name: "Editor 14", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 73, culture: 73, values: 74}},
        {user_id: "n16", favourite: false, name: "Editor 15", description: "Loren ipsum, dolor set amet", ref: "#", type: 'applied', percentage: {skills: 75, culture: 76, values: 77}},
        {user_id: "o17", name: "Editor 16", description: "Loren ipsum, dolor set amet", ref: "#", type: 'interviewing', percentage: {skills: 78, culture: 79, values: 80}}]

    constructor(props) {
        super(props);

        this.favouriteClicked = this.favouriteClicked.bind(this);

        this.state = {cards: [], status: "All candidate statuses"}
    }

    componentDidMount() {
        this.setState({cards: this.props.setup(this.cardsList, 12, "shortlist")})
    }

    componentWillUnmount() {
    }

    statusChanged = (event) =>  {
        this.setState({status: event.target.value});
    }

    favouriteClicked(newState, id, status) {
        const filtered = this.cardsList.filter(card => card.user_id === id)[0];
        filtered.favourite = newState
    }


    render() {

        const itemsList = this.state.cards.map((item, index) => <Card key={item.name} className="shadow border-0 pl-2 standard-height">{
            ((item.type == 'none' && (this.state.status == "All candidate statuses" || this.state.status == "No status")) && <InviteToApllyJobCard isInFavourite={item.favourite} favouriteClicked={this.favouriteClicked} animated={false} type={item.type}  card={item}/>)
        || ((item.type == 'applied' && (this.state.status == "All candidate statuses" || this.state.status == "Applied")) && <AcceptDeclinePerson animated={false} type={item.type}  card={item}/>)
        || ((item.type == 'hired'  && (this.state.status == "All candidate statuses" || this.state.status == "Hired")) && <InviteToApplyHired isInFavourite={item.favourite} favouriteClicked={this.favouriteClicked} animated={false} type={item.type}  card={item}/>)
        || ((item.type == 'interviewing' && (this.state.status == "All candidate statuses" || this.state.status == "Interviewing")) && <MakeOfferOrDeclinePerson animated={false} type={item.type}  card={item}/>)
        }</Card>)

        return (
            <section id="shortlist">
                <Container className="mt-4 mb-0">
                    <span className="shorlist-title">Shortlist</span>
                </Container>
                <Container className="pt-4 pb-4">
                        <div id="selectContainer">
                                <Input onChange={this.statusChanged} bsSize="md" type="select" name="select" className="select-element-style mb-2 mb-sm-2 mb-md-2 mb-lg-0 mb-xl-0">
                                    <option className="white">All candidate statuses</option>
                                    <option className="white">Applied</option>
                                    <option className="white">Hired</option>
                                    <option className="white">Interviewing</option>
                                    <option className="white">No status</option>
                                </Input>
                            <div className="empty-div"></div>
                                <Input bsSize="md" type="select" name="select" className="select-element-style">
                                    <option className="white">Jobs</option>
                                    <option className="white">2</option>
                                    <option className="white">3</option>
                                    <option className="white">4</option>
                                    <option className="white">5</option>
                                </Input>
                        </div>
                    <hr className="mb-3 mt-2"/>
                    <div className="grid-container">
                        {itemsList}
                    </div>
                </Container>
                <Container className="top-offset-for-pagination">
                    <Row>
                        <Col></Col>
                        <Col><PaginationComponent name="shortlist" maxPagesCount={Math.ceil(this.cardsList.length / 12)} /></Col>
                        <Col></Col>
                    </Row>
                </Container>
            </section>
        );
    }
}

export default (ShortlistPage);
