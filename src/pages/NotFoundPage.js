import React, { Component } from 'react';
import { Container,
    Row
} from 'reactstrap';

class NotFoundPage extends Component {

    render() {

        return (
            <section className="full-screen">
                <Container className="h-100">
                    <Row className="h-75 justify-content-center align-items-center">
                        <div className="text-center">
                            <h2>Page Not Found</h2>
                        </div>
                    </Row>
                </Container>
            </section>
        );
    }
}

export default NotFoundPage;