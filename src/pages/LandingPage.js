import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container, 
    Button, 
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink, 
    Row, 
    Col, 
    Card 
} from 'reactstrap';

import logo from '../assets/img/aspire-logo.svg'
import forbes from '../assets/img/landing/forbes.png'
import forbes2x from '../assets/img/landing/forbes@2x.png'
import forbes3x from '../assets/img/landing/forbes@3x.png'
import businessInsider from '../assets/img/landing/business-insider.png'
import businessInsider2x from '../assets/img/landing/business-insider@2x.png'
import businessInsider3x from '../assets/img/landing/business-insider@3x.png'
import britishAirways from '../assets/img/landing/british-airways.png'
import britishAirways2x from '../assets/img/landing/british-airways@2x.png'
import britishAirways3x from '../assets/img/landing/british-airways@3x.png'
import hdruk from '../assets/img/landing/hdruk.png'
import hdruk2x from '../assets/img/landing/hdruk@2x.png'
import hdruk3x from '../assets/img/landing/hdruk@3x.png'
import recruiter from '../assets/img/landing/recruiter.png'
import recruiter2x from '../assets/img/landing/recruiter@2x.png'
import recruiter3x from '../assets/img/landing/recruiter@3x.png'

import Footer from '../components/layout/Footer';

class LandingPage extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
      
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {

        let { isOpen } = this.state

        return (
            <React.Fragment>
                <section className="main-block full-screen">
                    <Container className="h-100">
                        <Navbar light expand="lg">
                            <NavbarBrand tag={Link} to="/">
                                <img src={logo} className="aspire-logo" alt="logo"/>
                            </NavbarBrand>
                            <NavbarToggler onClick={this.toggle} />
                            <Collapse isOpen={isOpen} navbar>
                                <Nav className="ml-auto align-items-center" navbar>
                                    <NavItem>
                                        <NavLink tag={Link} to="/">Home</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink tag={Link} to="#">Jobs</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink tag={Link} to="#">Candidates</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink tag={Link} to="#">Companies</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink tag={Link} to="#">Hubs</NavLink>
                                    </NavItem>
                                    <div className="vdivide"/>
                                    <NavItem>
                                        <NavLink tag={Link} className="btn-login" to="/login">LOGIN</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <Button color="primary" className="btn-sign-up" tag={Link} to={{pathname: '/signup', state: { type: "company" }}}>Sign Up</Button>
                                    </NavItem>
                                </Nav>
                            </Collapse>
                        </Navbar>
                        <Row className="h-75 justify-content-center align-items-center">
                            <div className="text-center">
                                <h2>Find Your Perfect Employee Match For Working</h2>
                                <h5>Let us inspire you</h5>
                                <div className="text-center">
                                    <Button color="primary" size="lg" tag={Link} to={{pathname: '/signup', state: { type: "company" }}}>Create account</Button>{' '}
                                    <Button outline color="primary" size="lg" tag={Link} to={{pathname: '/signup', state: { type: "candidate" }}}>I'm candidate</Button>
                                </div>
                            </div>
                        </Row>
                    </Container>
                </section>
                <section className="second-block">
                    <Container>
                        <Row className="align-items-center">
                            <Col md="5" xs="12">
                                <div>
                                    <h3>How It Works</h3>
                                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>
                                </div>
                            </Col>
                            <Col md="1"/>
                            <Col md="6" xs="12">
                                <div className="empty-box"/>
                            </Col>
                        </Row>
                        <Row className="align-items-center">
                            <Col md="6" xs="12">
                                <div className="empty-box"/>
                            </Col>
                            <Col md="1"/>
                            <Col md="5" xs="12">
                                <div>
                                    <h3>Perfect Matching Among 1200+ Verified Candidates</h3>
                                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="third-block">
                    <Container className="text-center">
                        <h3>Our Clients</h3>
                        <Row className="align-items-center">
                            <Col>
                                <img src={forbes} srcSet={`${forbes2x} 2x, ${forbes3x} 3x`} className="forbes-logo" alt="forbes-logo"/>
                            </Col>
                            <Col>
                                <img src={businessInsider} srcSet={`${businessInsider2x} 2x, ${businessInsider3x} 3x`} className="business-insider-logo" alt="business-insider-logo"/>
                            </Col>
                            <Col>
                                <img src={britishAirways} srcSet={`${britishAirways2x} 2x, ${britishAirways3x} 3x`} className="british-airways-logo" alt="british-airways-logo"/>
                            </Col>
                            <Col>
                                <img src={hdruk} srcSet={`${hdruk2x} 2x, ${hdruk3x} 3x`} className="hdruk-logo" alt="hdruk-logo"/>
                            </Col>
                            <Col>
                                <img src={recruiter} srcSet={`${recruiter2x} 2x, ${recruiter3x} 3x`} className="recruiter-logo" alt="recruiter-logo"/>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="fourth-block">
                    <Container>
                        <Card>
                            <Row className="align-items-center">
                                <Col lg="9" md="6" xs="12">
                                    <h3>Are you ready for change?</h3>
                                    <h5>Create company account and find the perfect matching</h5>
                                </Col>
                                <Col lg="3" md="6" xs="12">
                                    <Button color="primary" size="lg" tag={Link} to={{pathname: '/signup', state: { type: "company" }}}>Create account</Button>
                                </Col>
                            </Row>
                        </Card>
                    </Container>
                </section>
                <Footer />
            </React.Fragment>
        );
    }
}

export default LandingPage;
