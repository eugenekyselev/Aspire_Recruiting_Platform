import { /*call, */put, all, take } from "redux-saga/effects";
import { startSubmit, stopSubmit, reset } from 'redux-form'
import * as constants from '../constants/actionTypes';
import { push } from 'connected-react-router'

export function* rootSaga() {
    yield all([
        candidateLoginFlow(),
        companyLoginFlow(),
        linkedInAuthFlow(),
        logoutFlow(),
        candidateSignUpFlow(),
        companySignUpFlow(),
        resetPasswordFlow()
    ])
}

function* resetPasswordFlow(){
    while (true) {

        let { /*email, */formId } = yield take(constants.RESET_PASSWORD_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, email)
            yield put({type: constants.SET_RESET_PASSWORD_ALERT, alert: "Description with instructions was sent to your email"})
            
            yield put(reset(formId))
            yield put(stopSubmit(formId))
        } catch (error) {
            yield put({type: constants.SET_RESET_PASSWORD_ERROR, alert: error.message}) 
            yield put(stopSubmit(formId))
        }
    }
}

function* linkedInAuthFlow(){
    while (true) {

        let { lastname, firstname, email, avatar, formId } = yield take(constants.LINKEDIN_AUTH_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, lastname, firstname, email, password)
            yield put({type: constants.SET_CANDIDATE_DATA, payload: {"id": "someId", "firstname": firstname, "lastname": lastname, "email": email, "avatar": avatar}})
            
            localStorage.setItem("role", "candidate");
            localStorage.setItem("data", JSON.stringify({"id": "someId", "firstname": firstname, "lastname": lastname, "email": email, "avatar": avatar}));

            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* candidateLoginFlow() {
    while (true) {

        let { /*email, password, */formId } = yield take(constants.CANDIDATE_LOGIN_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, email, password)
            yield put({type: constants.SET_CANDIDATE_DATA, payload: {"id": "someId", "firstname": "Jack", "lastname": "Cook", "email": "jackcook@mail.com"}})
            
            localStorage.setItem("role", "candidate");
            localStorage.setItem("data", JSON.stringify({"id": "someId", "firstname": "Jack", "lastname": "Cook", "email": "jackcook@mail.com"}));

            yield put(reset(formId))
            yield put(stopSubmit(formId))

            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* companyLoginFlow() {
    while (true) {

        let { /*email, password, */formId } = yield take(constants.COMPANY_LOGIN_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, email, password)
            yield put({type: constants.SET_COMPANY_DATA, payload: {"id": "someId", "firstname": "Sandra", "lastname": "Limb", "company": "Google Inc", "email": "jackcook@mail.com"}})

            localStorage.setItem("role", "company");
            localStorage.setItem("data", JSON.stringify({"id": "someId", "firstname": "Sandra", "lastname": "Limb", "company": "Google Inc", "email": "jackcook@mail.com"}));

            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* logoutFlow() {
    while (true) {

        yield take(constants.LOGOUT_REQUEST)

        yield put({type: constants.RESET_DATA})

        localStorage.clear();

        yield put(push('/login'));
    }
}

function* candidateSignUpFlow() {
    while (true) {

        let { /*lastname, firstname, email, password, */formId } = yield take(constants.CANDIDATE_SIGNUP_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, lastname, firstname, email, password)
            yield put({type: constants.SET_CANDIDATE_DATA, payload: {"id": "someId", "firstname": "Jack", "lastname": "Cook", "email": "jackcook@mail.com"}})
            
            localStorage.setItem("role", "candidate");
            localStorage.setItem("data", JSON.stringify({"id": "someId", "firstname": "Jack", "lastname": "Cook", "email": "jackcook@mail.com"}));

            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/profile"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}

function* companySignUpFlow() {
    while (true) {

        let { /*lastname, firstname, company, email, password, */formId } = yield take(constants.COMPANY_SIGNUP_REQUEST)

        const delay = (ms) => new Promise(res => setTimeout(res, ms))
        
        yield put(startSubmit(formId))

        try {

            yield delay(2000)
            
            // const someId = yield call(api, lastname, firstname, company, email, password)
            yield put({type: constants.SET_COMPANY_DATA, payload: {"id": "someId", "firstname": "Sandra", "lastname": "Limb", "company": "Google Inc", "email": "jackcook@mail.com"}})
            
            localStorage.setItem("role", "company");
            localStorage.setItem("data", JSON.stringify({"id": "someId", "firstname": "Sandra", "lastname": "Limb", "company": "Google Inc", "email": "jackcook@mail.com"}));

            yield put(reset(formId))
            yield put(stopSubmit(formId))
            
            yield put(push("/assessment"))
        } catch (error) {
            yield put(stopSubmit(formId, { _error: error.message }))  
        }
    }
}