import * as constants from '../constants/actionTypes';

// login
export const loginCandidate = (email, password, formId) => ({ type: constants.CANDIDATE_LOGIN_REQUEST, email: email, password: password, formId: formId });
export const linkedInAuth = (firstname, lastname, email, avatar, formId) => ({ type: constants.LINKEDIN_AUTH_REQUEST, firstname: firstname, lastname: lastname, email: email, avatar: avatar, formId: formId });
export const loginCompany = (email, password, formId) => ({ type: constants.COMPANY_LOGIN_REQUEST, email: email, password: password, formId: formId });

// signup
export const signUpCandidate = (firstname, lastname, email, password, formId) => ({ type: constants.CANDIDATE_SIGNUP_REQUEST, firstname: firstname, lastname: lastname, email: email, password: password, role: "candidate", formId: formId });
export const signUpCompany = (firstname, lastname, company, email, password, formId) => ({ type: constants.COMPANY_SIGNUP_REQUEST, firstname: firstname, lastname: lastname, company: company, email: email, password: password, role:"company", formId: formId });

// reset password
export const resetPassword = (email, formId) => ({ type: constants.RESET_PASSWORD_REQUEST, email: email, formId: formId });
export const clearResetPassword = () => ({ type: constants.CLEAR_RESET_PASSWORD });

// logout
export const logout = () => ({ type: constants.LOGOUT_REQUEST });

// assessment
export const setCulture = (culture) => ({type: constants.SET_CULTURE_ASSESSMENT, culture: culture});
export const setPersonalities = (personalities) => ({type: constants.SET_PERSONALITIES_ASSESSMENT, personalities: personalities});
export const setValues = (values) => ({type: constants.SET_VALUES_ASSESSMENT, values: values});

export const changeCulturePage = (page) => ({type: constants.CHANGE_CULTURE_ASSESSMENT_PAGE, page: page});
export const changePersonalitiesPage = (page) => ({type: constants.CHANGE_PERSONALITIES_ASSESSMENT_PAGE, page: page});
export const changeValuesPage = (page) => ({type: constants.CHANGE_VALUES_ASSESSMENT_PAGE, page: page});

export const clearCulture = () => ({type: constants.CLEAR_CULTURE_ASSESSMENT});
export const clearPersonalities = () => ({type: constants.CLEAR_PERSONALITIES_ASSESSMENT});
export const clearValues = () => ({type: constants.CLEAR_VALUES_ASSESSMENT});

export const clearAssessment = () => ({type: constants.CLEAR_ASSESSMENT});

// errors
export const setError = (error) => ({type: constants.SET_ERROR, payload: error});
export const resetError = () => ({type: constants.RESET_ERROR_MESSAGE});

// profile page
export const changeProfilePage = (page) => ({type:constants.CHANGE_PROFILE_PAGE, page:page})

export const setPage = (page) => ({type: constants.SAVE_PAGE, startPosition: page});
export const setActivePage = (page) => ({type: constants.SAVE_ACTIVE_PAGE, activePosition: page});