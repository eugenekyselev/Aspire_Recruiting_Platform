import * as constants from '../constants/actionTypes';

const initialState = {
    startPosition: [],
    activePosition: []
}

export default function paginationtReducer(state = initialState, action) {

    switch (action.type) {
        case constants.SAVE_PAGE:

            if (state.startPosition instanceof Array) {
                let existStart = state.startPosition.filter(elem => elem.pagename == action.startPosition.pagename)

                if (existStart.length > 0) {
                    existStart[0].value = action.startPosition.value
                    return {...state}
                }
            }

            let resultForStart = { ...state}
            resultForStart.startPosition.push(action.startPosition)
            return {...resultForStart}
        case constants.SAVE_ACTIVE_PAGE:

            if (state.activePosition instanceof Array) {
                let existActive = state.activePosition.filter(elem => elem.pagename == action.activePosition.pagename)
                if (existActive.length > 0) {
                    existActive[0].value = action.activePosition.value
                    return {...state}
                }
            }

            let result = { ...state}
            result.activePosition.push(action.activePosition)
            return {...result}
        default:
            return state;
    }
}
