import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import errorReducer from './errors'
import userReducer from './user'
import resetPasswordReducer from './resetPassword'
import assessmentReducer from './assessment'
import paginationtReducer from './pagination'
import profilePageReducer from './profilePage'

const reducer = combineReducers({
    form: formReducer,
    error: errorReducer,
    user: userReducer,
    resetPassword: resetPasswordReducer,
    assessment: assessmentReducer,
    paginationtReducer: paginationtReducer,
    profilePage: profilePageReducer
})

export default reducer