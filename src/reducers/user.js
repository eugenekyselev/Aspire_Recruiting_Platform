import * as constants from '../constants/actionTypes';

const initialState = {
    role: localStorage.getItem("role") === null ? "" : localStorage.getItem("role"),
    data: localStorage.getItem("data") === null ? null : JSON.parse(localStorage.getItem("data"))
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
    case constants.SET_COMPANY_DATA:{
        return {
            role: "company",
            data: action.payload
        }
    }
    case constants.SET_CANDIDATE_DATA:{
        return {
            role: "candidate",
            data: action.payload
        }
    }
    case constants.RESET_DATA:{
        return {
            role: "",
            data: null
        };
    }
    default:
        return state;
    }
}
    
export default userReducer