import * as constants from '../constants/actionTypes';

const errorsReducer = (state = null, action) => {
    switch (action.type) {
    case constants.RESET_ERROR_MESSAGE:{
        return null
    }
    case constants.SET_ERROR:{
        return action.payload
    }
    case constants.RESET_DATA:{
        return null
    }
    default:
        return state;
    }
}
    
export default errorsReducer