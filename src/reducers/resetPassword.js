import * as constants from '../constants/actionTypes';

const initialState = {
    alert: {}
}

const resetPasswordReducer = (state = initialState, action) => {
    switch (action.type) {
    case constants.SET_RESET_PASSWORD_ALERT:{
        return {
            alert: {
                error: false,
                message: action.alert
            }
        }
    }
    case constants.SET_RESET_PASSWORD_ERROR:{
        return {
            alert: {
                error: true,
                message: action.alert
            }
        }
    }
    case constants.CLEAR_RESET_PASSWORD:{
        return initialState
    }
    case constants.RESET_DATA:{
        return initialState
    }
    default:
        return state;
    }
}
    
export default resetPasswordReducer