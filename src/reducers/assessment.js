import * as constants from '../constants/actionTypes';

const initialState = {
    pages: {
        culture: 0,
        personalities: 0,
        values: 0
    },
    culture: {},
    personalities: {},
    values: {}
}

const assessmentReducer = (state = initialState, action) => {
    switch (action.type) {
    case constants.SET_CULTURE_ASSESSMENT:{
        return {
            ...state,
            culture: action.culture
        }
    }
    case constants.SET_PERSONALITIES_ASSESSMENT:{
        return {
            ...state,
            personalities: action.personalities
        }
    }
    case constants.SET_VALUES_ASSESSMENT:{
        return {
            ...state,
            values: action.values
        }
    }
    case constants.CHANGE_CULTURE_ASSESSMENT_PAGE:{
        return {
            ...state,
            pages: {
                ...state.pages,
                culture: action.page
            }
        }
    }
    case constants.CHANGE_PERSONALITIES_ASSESSMENT_PAGE:{
        return {
            ...state,
            pages: {
                ...state.pages,
                personalities: action.page
            }
        }
    }
    case constants.CHANGE_VALUES_ASSESSMENT_PAGE:{
        return {
            ...state,
            pages: {
                ...state.pages,
                values: action.page
            }
        }
    }
    case constants.CLEAR_CULTURE_ASSESSMENT:{
        return {
            ...state,
            pages: {
                ...state.pages,
                culture:0
            },
            culture: initialState.culture
        }
    }
    case constants.CLEAR_PERSONALITIES_ASSESSMENT:{
        return {
            ...state,
            pages: {
                ...state.pages,
                personalities:0
            },
            personalities: initialState.personalities
        }
    }
    case constants.CLEAR_VALUES_ASSESSMENT:{
        return {
            ...state,
            pages: {
                ...state.pages,
                values:0
            },
            values: initialState.values
        }
    }
    case constants.CLEAR_ASSESSMENT:{
        return initialState;
    }
    case constants.RESET_DATA:{
        return initialState;
    }
    default:
        return state;
    }
}
    
export default assessmentReducer