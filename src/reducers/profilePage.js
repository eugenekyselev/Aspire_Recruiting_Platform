import * as constants from '../constants/actionTypes';

const initialState = {
    page: "form"
}

const profilePageReducer = (state = initialState, action) => {
    switch (action.type) {
    case constants.CHANGE_PROFILE_PAGE:{
        return {
            page: action.page
        }
    }
    case constants.RESET_DATA:{
        return initialState
    }
    default:
        return state;
    }
}
    
export default profilePageReducer