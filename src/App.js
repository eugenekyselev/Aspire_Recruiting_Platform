import React, { Component } from 'react';
import { Switch } from 'react-router-dom'
import { UserIsAuthenticated, UserIsNotAuthenticated, userIsCompanyChain } from './wrappers';
// layouts
import LayoutRoute from './components/layout/LayoutRoute';
import EmptyLayout from './components/layout/EmptyLayout';
import AppLayout from './components/layout/AppLayout';
import PaginatedAppLayout from './components/layout/PaginatedAppLayout'
// pages
import LandingPage from './pages/LandingPage';
import SignUpPage from './pages/SignUpPage';
import LoginPage from './pages/LoginPage';
import ResetPasswordPage from './pages/ResetPasswordPage';
import AssessmentPage from './pages/AssessmentPage';
import ProfilePage from './pages/ProfilePage';
import CompanyProfilePreview from "./components/profiles/CompanyProfilePreview";

import JobsPage from './pages/jobs/JobsPage';
import CreateJobPage from './pages/jobs/CreateJobPage';
import NotFoundPage from './pages/NotFoundPage';
import JobsListPage from './pages/jobs/JobsListPage';
import ViewJobPage from './pages/jobs/ViewJobPage';
import ShortlistPage from './pages/ShortlistPage';
import PageWithPagination from './pages/base/PageWithPagination';

class App extends Component {
    render() {
        return (
            <Switch>
                <LayoutRoute exact path="/" layout={EmptyLayout} component={LandingPage} />
                <LayoutRoute exact path="/signup" layout={EmptyLayout} component={UserIsNotAuthenticated(SignUpPage)} />
                <LayoutRoute exact path="/login" layout={EmptyLayout} component={UserIsNotAuthenticated(LoginPage)} />
                <LayoutRoute exact path="/reset-password" layout={EmptyLayout} component={UserIsNotAuthenticated(ResetPasswordPage)} />
                <LayoutRoute exact path="/assessment" layout={AppLayout} component={UserIsAuthenticated(AssessmentPage)} />
                <LayoutRoute exact path="/profile" layout={AppLayout} component={UserIsAuthenticated(ProfilePage)} />
                <LayoutRoute exact path="/shortlist" layout={PaginatedAppLayout} component={UserIsAuthenticated(ShortlistPage)} />
                <LayoutRoute exact path="/jobs" layout={PaginatedAppLayout} component={UserIsAuthenticated(JobsListPage)} />
                <LayoutRoute exact path="/jobs/view" layout={AppLayout} component={UserIsAuthenticated(ViewJobPage)} />
                <LayoutRoute exact path="/company-preview" layout={AppLayout} component={UserIsAuthenticated(CompanyProfilePreview)} />
                <LayoutRoute layout={AppLayout} component={NotFoundPage} />
               
            </Switch>
        );
    }
}

export default App;